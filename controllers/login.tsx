import { NextApiRequest, NextApiResponse } from "next";

const post = async function (
    req: NextApiRequest,
    res: NextApiResponse,
) {
    try {
        const url = `${process.env.SERVER_HOST ?? 'localhost'}/api/login`;
        await fetch(new URL(url), {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json",
            },
            body: JSON.stringify(req.body),
        }).then(async (response) => {
            let body = await response.json();

            res.status(response.status).json(body)
        });
    } catch (error) {
        res.status(500).json(error)
    }
};

const methodsMap: {
    [key: string]: (req: NextApiRequest, res: NextApiResponse) => any;
} = {
    post,
};

export default function loginController(
    req: NextApiRequest,
    res: NextApiResponse,
) {
    if (!req.method) {
        throw "Unexpected request error";
    }
    const method = req.method.toLowerCase();

    if (!(method in methodsMap)) {
        return res.status(501).end();
    }

    return methodsMap[method](req, res);
}