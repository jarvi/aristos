import { FileTemplateNames, Lenguajes } from "../util/constants";
const DownloadDoc = (e: any) => {
  const selectLenguaje = window?.location?.pathname;
  const elementId = e?.currentTarget?.id;
  const filePaths: any = {
    [FileTemplateNames.TemplateOne]: `/files/${Lenguajes[selectLenguaje]}/PlantillasEvaluacion.docx`,
    [FileTemplateNames.TemplateTwo]: `/files/${Lenguajes[selectLenguaje]}/PlantillasEvaluacion.xlsx`,
    [FileTemplateNames.TemplateThree]: `/files/${Lenguajes[selectLenguaje]}/SolicitudFinanciacion.docx`,
    [FileTemplateNames.TemplateFour]: `/files/${Lenguajes[selectLenguaje]}/SolicitudFinanciacion.xlsx`,
    [FileTemplateNames.TemplateFive]: `/files/${Lenguajes[selectLenguaje]}/KYC.docx`,
    [FileTemplateNames.TemplateSix]: `/files/${Lenguajes[selectLenguaje]}/KYC.xlsx`,
    [FileTemplateNames.TemplateSeven]: `/files/${Lenguajes[selectLenguaje]}/CompartirInformacion.docx`,
    [FileTemplateNames.TemplateEight]: `/files/${Lenguajes[selectLenguaje]}/CompartirInformacion.xlsx`,
    [FileTemplateNames.TemplateNine]: `/files/${Lenguajes[selectLenguaje]}/DeclaracionJurada.docx`,
    [FileTemplateNames.TemplateTen]: `/files/${Lenguajes[selectLenguaje]}/DeclaracionJurada.xlsx`,
  };

  const currentHref = filePaths[elementId];

  //console.log(currentHref);
  
  if (currentHref) {
    const link = document.createElement("a");
    link.download = elementId;
    link.href = currentHref;
    link.click();
  }
};
export default DownloadDoc;
