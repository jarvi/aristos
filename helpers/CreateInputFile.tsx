const CreateInputFile = (id: any) => {
  const input = document.createElement("input");
  input.setAttribute("type", "file");
  input.setAttribute("id", `${id}`);
  input.click();
  return input;
};
export default CreateInputFile;
