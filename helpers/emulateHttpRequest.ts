export default function emulateHttpRequest(objectUsedForHttpRequest: any) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (objectUsedForHttpRequest) {
        resolve(false);
      }
      resolve(true);
    }, 3000);
  });
}
