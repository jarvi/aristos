import React from "react";

export default function Video() {
  return (
    <>
      <video width="100%"  controls>
        <source
          src="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4"
          type="video/mp4"
        />
        <source
          src="https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/1080/Big_Buck_Bunny_1080_10s_1MB.mp4"
          type="video/mp4"
        />

        {/* <source
          src="/video/test.mp4"
          type="video/mp4"
        /> */}
      </video>
    </>
  );
}
