import { faX } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useContext, useState } from "react";
import useGlobalUserData from "../../hooks/useGlobalUserData";

import InformativeModal from "../informativeModal/InformativeModal";
import ModalContext from "../../contexts/ModalContext";
import Login from "../auth/Login";
import ResetPassword from "../auth/ResetPassword";
import SignUp from "../auth/SignUp";
import { useTranslations } from "next-intl";

type Props = {
  children: JSX.Element;
  buttonType: string;
  text: string;
  eventCallback?: Function;
};

const UserStatus = {
  LoggedIn: "LOGGED_IN",
  LoggedOut: "LOGGED_OUT",
};

export default function Modal() {
  const t = useTranslations("Layout");

  const {
    showModal: modalState,
    setShowModal,
    selectedForm,
    setSelectedForm,
  } = useContext(ModalContext);
  const { userStatus, user } = useGlobalUserData();
  const [btnType, setBtnType] = useState(
    "block w-40 p-2 text-sm text-center text-gray-500 border border-gray-200 rounded-lg bg-gray-200"
  );

  const [textBtn, setTextBtn] = useState(
    userStatus == "LOGGED_IN" ? user?.name : `${t("enter")}`
  );

  const renderSelectedForm = () => {
    switch (selectedForm) {
      case "loginForm":
        return <Login />;
      case "resetPasswordForm":
        return <ResetPassword />;
      case "registerForm":
        return <SignUp />;
      case "informativeModal":
        return <InformativeModal />;
    }
  };

  const handleModal = () => {
    if (userStatus === "LOGGED_IN") {
      setShowModal(false);
    }

    if (!(userStatus === "LOGGED_IN")) {
      setShowModal(true);
      setSelectedForm("loginForm");
    }
  };

  const handleLogout = () => {
    setSelectedForm("informativeModal");
    setShowModal(true);
  };

  return (
    <>
      {userStatus === "LOGGED_IN" && (
        <button className={btnType} type="button" onClick={handleLogout}>
          {"Logout"}
        </button>
      )}
      <button className={btnType} type="button" onClick={handleModal}>
        {textBtn}
      </button>
      {modalState ? (
        <>
          <div className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div className="relative w-auto mx-auto max-w-4xl">
              {/*content*/}
              <div className="border-2 border-slate-400 shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*close button*/}
                <button
                  className="absolute z-20 top-4 right-4 p-1 bg-transparent border-blue-900 border-2 rounded-full text-blue-900 text-3xl leading-none font-bold outline-none focus:outline-none"
                  onClick={() => {
                    setShowModal(false);
                    //console.log("cerrando el modal ");
                  }}
                >
                  <FontAwesomeIcon className="h-5 w-5" icon={faX} />
                </button>
                {/*body*/}
                {/* {children} */}
                  {renderSelectedForm()}
                {/*body*/}
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}
