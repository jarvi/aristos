import { faFacebook, faGooglePlus, faLinkedin, faTwitter, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Head from 'next/head';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import AuthModal from '../auth/AuthModal';
import Alert from './Alert';
import { useTranslations } from 'next-intl';
import { useEffect } from 'react';
import Modal from './Modal';



type Props = {
    children: JSX.Element,
}

export default function Layout({ children }: Props) {
    const t = useTranslations('Layout');
    const { locale, asPath } = useRouter();

    return (
        <>
            <Head>
                <title>Aristos</title>
                <meta name="description" content="Aristos web" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <div style={{
                maxWidth: '1366px',
                margin: '0px auto'
                }} className='flex flex-col  bg-zinc-50'>
                <header>
                    <div className='bg-gray-300 w-full px-12 py-4'>
                        <span className='text-sm italic text-gray-600'>
                            {t('title')}
                        </span>
                    </div>
                    <nav className="font-helios-antique w-full">
                        <div className="flex flex-wrap w-full items-center justify-between bg-white px-12 py-8">
                            <Link href="/" className="flex items-center justify-start">
                                <Image width={220} height={50} priority src="/img/logo-horizontal-full-color.png" className="h-12 w-auto" alt="Logo" />
                            </Link>
                            <div className='flex'>
                                <div className="flex mr-20">
                                    <div className='mr-1.5'>
                                        <button type="button" className="w-10 h-10 m-auto text-white text-sm p-2 rounded-full bg-gray-400" >
                                            <FontAwesomeIcon className='w-5 h-5 m-auto' icon={faUser} />
                                        </button>
                                    </div>
                                    <div style={{
                                        display: 'flex',
                                        gap: '10px'
                                    }} className="relative ">
                                        <Modal />
                                    </div>
                                </div>
                                <div className="flex">
                                    <Link href={`${asPath}`} locale="en" className="mr-1.5 rounded-full" >
                                        <Image width={32} height={32} src="/img/flag_icon_round_usa.png" alt="USA flag" />
                                    </Link>
                                    <Link href={`${asPath}`} locale="fr" className="mr-1.5 rounded-full" >
                                        <Image width={32} height={32} src="/img/flag_icon_round_france.svg" alt="France flag" />
                                    </Link>
                                    <Link href={`${asPath}`} locale="es" className="mr-1.5 rounded-full" >
                                        <Image width={32} height={32} src="/img/flag_icon_round_spain.svg" alt="Spain flag" />
                                    </Link>
                                </div>
                            </div>
                        </div>
                        <div className='navbar-container-desktop items-center justify-between flex order-1 bg-blue-900 w-full px-20 py-4'>

                            <ul className=" ul-main flex flex-row space-x-2 xl:space-x-8 mt-0 text-sm font-medium border-0">
                                <li className={ asPath === '/us' ?  'border-b-2 border-slate-200': ''}  >
                                    <Link href="/aboutUs" className={ asPath === '/aboutUs' ?  'block py-2 ml-3 mr-4 font-bold text-slate-200': 'block py-2 ml-3 mr-4 font-normal text-slate-200'} > {  t('navbar.us')  } </Link>
                                </li>
                                <li className={ asPath === '/emitter' ?  'border-b-2 border-slate-200': ''}>
                                    <Link href="/emitter" className={ asPath === '/emitter' ?  'block py-2 ml-3 mr-4 font-bold text-slate-200': 'block py-2 ml-3 mr-4 font-normal text-slate-200'}  >{t('navbar.emitter')}</Link>
                                </li>
                                <li>
                                    <Link href="#" className="block py-2 ml-3 mr-4 font-normal text-slate-200">{t('navbar.marketplace')}</Link>
                                </li>
                                <li className= { asPath === '/funding' ?  'border-b-2 border-slate-200': ''}>
                                    <Link href="/funding" className={ asPath === '/funding' ?  'block py-2 ml-3 mr-4 font-bold text-slate-200': 'block py-2 ml-3 mr-4 font-normal text-slate-200'}>{t('navbar.funding')}</Link>
                                </li>
                                <li>
                                    <Link href="#" className="block py-2 ml-3 mr-4 font-normal text-slate-200">{t('navbar.investor')}</Link>
                                </li>
                                <li className= {  asPath === '/projects' || asPath === '/projects/new' ?  'border-b-2 border-slate-200': ''} >
                                    <Link href="/projects" className={ asPath === '/projects' || asPath === '/projects/new' ?  'block py-2 ml-3 mr-4 font-bold text-slate-200': 'block py-2 ml-3 mr-4 font-normal text-slate-200'}    >{t('navbar.projects')}</Link>
                                </li>
                            </ul>

                            <button className='px-8 xl:px-12 py-1 rounded-md font-600 text-base xl:text-lg italic border-r-gray-400 bg-slate-100 text-blue-900'>
                                {t('contactUs')}
                            </button>
                        </div>
                    </nav>
                </header>

                <main className='flex-1'>{children}</main>

                <footer className='font-helios-antique w-full'>
                    <div className="bg-[url('/img/foto1.jpg')] bg-cover mb-8 px-20 py-6">
                        <div className="container w-full flex items-center justify-around ">
                            <Link href="/" className="flex items-center justify-start mr-6">
                                <Image width={220} height={50} src="/img/logo-horizontal-blanco.png" className="h-12 w-auto" alt="Aristos Logo" />
                            </Link>
                            <span className='pl-12 py-12 self-center border-l-2 border-white text-lg font-light text-white'>
                                {t('title')}
                            </span>
                            <button className='px-12 py-1 rounded-md font-normal text-xl italic border-r-gray-400 bg-slate-100 text-blue-900'>
                                {t('contactUs')}
                            </button>
                        </div>
                    </div>
                    <div className="grid grid-cols-6 w-full py-20 px-14 bg-blue-900 text-white font-helios-antique">
                        <div className="col-span-1 xl:col-span-2 flex flex-col items-start text-xs pr-12">
                            <Image width={220} height={50} src="/img/logo-horizontal-blanco.png" alt="Aristos Logo" className="h-6 xl:h-10 w-auto mb-6" />
                            <p>
                                {t('footer.aboutUs')}
                            </p>
                            <div className='mt-4 flex'>
                                <div>
                                    <FontAwesomeIcon className='w-5 h-5 mr-2' icon={faMapMarkerAlt} />
                                </div>
                                <p>
                                    {t('footer.location')}
                                </p>
                            </div>
                        </div>
                        <div className="col-span-4 xl:col-span-3 grid grid-cols-3 text-xs px-8 border-l-2 border-white">
                            <ul className="flex flex-col mx-4">
                                <li className='text-sm font-semibold mb-4'>{t('footer.ul.li')}</li>
                                <li className='font-light mb-4'>{t('footer.ul.li_1')}</li>
                                <li className='font-light mb-4'>{t('footer.ul.li_2')}</li>
                                <li className='font-light mb-4'>Cookies</li>
                                <li className='font-light'>{t('footer.ul.li_4')}</li>
                            </ul>
                            <ul className="flex flex-col mx-4">
                                <li className='text-sm font-semibold mb-4'>{t('footer.ul_1.li')}</li>
                                <li className='font-light mb-4'>{t('footer.ul_1.li_1')}</li>
                                <li className='font-light mb-4'>{t('footer.ul_1.li_2')}</li>
                                <li className='font-light mb-4'>{t('footer.ul_1.li_3')}</li>
                                <li className='font-light mb-4'>FAQ</li>
                                <li className='font-light mb-4'>{t('footer.ul_1.li_5')}</li>
                            </ul>
                            <div className="flex flex-col mx-4">
                                <h3 className='text-sm font-semibold border-b-2 border-white pb-2'>{t('footer.h3')}</h3>
                                <div className='flex mt-4'>
                                    <FontAwesomeIcon className='w-5 h-5 mr-2' icon={faWhatsapp} />
                                    <ul>
                                        <li>+503 0000-0000</li>
                                        <li>+503 0000-0000</li>
                                        <li>+503 0000-0000</li>
                                        <li>+503 0000-0000</li>
                                        <li>+503 0000-0000</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col-span-1 text-center">
                            <ul>
                                <li><FontAwesomeIcon className='w-8 h-8 mb-6' icon={faFacebook} /></li>
                                <li><FontAwesomeIcon className='w-8 h-8 mb-6' icon={faTwitter} /></li>
                                <li><FontAwesomeIcon className='w-8 h-8 mb-6' icon={faGooglePlus} /></li>
                                <li><FontAwesomeIcon className='w-8 h-8' icon={faLinkedin} /></li>
                            </ul>
                        </div>
                    </div>
                    <div className='bg-gray-300 w-full text-center px-12 py-4'>
                        <span className='text-xs text-blue-600'>
                           {t('footer.span')}
                        </span>
                    </div>
                </footer>
            </div>
            <Alert />
        </>
    );
};