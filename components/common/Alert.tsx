import { faBell } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useContext } from "react";
import AlertContext from "../../contexts/AlertContext";

export default function Alert() {
    const alert = useContext(AlertContext);
    if (alert.alert !== 'NONE') {
        return (
            <div
                className={`fixed flex top-12 left-12 z-50 text-white px-6 py-4 max-w-sm border-0 rounded mb-4 ${alert.alert == 'SUCCESS' ? 'bg-green-500' : 'bg-red-500'}`}
            >
                <span className="text-xl mr-5 place-self-center">
                    <FontAwesomeIcon className="w-5 h-5" icon={faBell} />
                </span>
                <span className="align-middle mr-8">
                    {alert.alertText}
                </span>
            </div>);
    } else {
        return null;
    }
}