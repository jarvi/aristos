import Image from "next/image";
import Link from "next/link";
import React, { RefObject, useContext } from "react";
import { useRouter } from "next/router";
import AlertContext from "../../contexts/AlertContext";
import useGlobalUserData from "../../hooks/useGlobalUserData";

type Props = {
  slides: Array<any>;
};

function FullWidthCarousel({ slides }: Props) {
  // We will start by storing the index of the current image in the state.
  const [currentSlide, setCurrentSlide] = React.useState(0);
  const router = useRouter();
  const alert = useContext(AlertContext);
  const { userStatus } = useGlobalUserData();

  // We are using react ref to 'tag' each of the images. Below will create an array of
  // objects with numbered keys. We will use those numbers (i) later to access a ref of a
  // specific image in this array.
  const refs: RefObject<any>[] = slides.reduce(
    (acc: any, _val: any, i: number) => {
      acc[i] = React.createRef();
      return acc;
    },
    {}
  );

  //alert.error(result.message);

  const scrollToSlide = (i: number) => {
    // First let's set the index of the image we want to see next
    setCurrentSlide(i);
    // Now, this is where the magic happens. We 'tagged' each one of the images with a ref,
    // we can then use built-in scrollIntoView API to do eaxactly what it says on the box - scroll it into
    // your current view! To do so we pass an index of the image, which is then use to identify our current
    // image's ref in 'refs' array above.
    refs[i].current.scrollIntoView({
      //     Defines the transition animation.
      behavior: "smooth",
      //      Defines vertical alignment.
      block: "nearest",
      //      Defines horizontal alignment.
      inline: "start",
    });
  };

  // Some validation for checking the array length could be added if needed
  const totalSlides = slides.length;

  // Below functions will assure that after last image we'll scroll back to the start,
  // or another way round - first to last in previousSlide method.
  const nextSlide = () => {
    if (currentSlide >= totalSlides - 1) {
      scrollToSlide(0);
    } else {
      scrollToSlide(currentSlide + 1);
    }
  };

  const previousSlide = () => {
    if (currentSlide === 0) {
      scrollToSlide(totalSlides - 1);
    } else {
      scrollToSlide(currentSlide - 1);
    }
  };

  const handleRedirection = () => {
    if (userStatus === "LOGGED_IN") {
      router.push("/projects/new");
    }
    if (!(userStatus === "LOGGED_IN")) {
      alert.error("Debe hacer login para acceder a esta sección");
    }
  };

  return (
    // Images are placed using inline flex. We then wrap an image in a div
    // with flex-shrink-0 to stop it from 'shrinking' to fit the outer div.
    // Finally the image itself will be 100% of a parent div. Outer div is
    // set with position relative, so we can place our control buttons using
    // absolute positioning on each side of the image.
    <div className="relative overflow-hidden">
      <div className="inline-flex overflow-x-hidden snap-x snap-mandatory">
        <button
          type="button"
          onClick={previousSlide}
          className="absolute top-0 left-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
        >
          <span className="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
            <svg
              aria-hidden="true"
              className="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M15 19l-7-7 7-7"
              ></path>
            </svg>
            <span className="sr-only">Previous</span>
          </span>
        </button>
        {slides.map((slide, index) => (
          <div
            className="w-full flex-shrink-0 relative"
            key={index}
            ref={refs[index]}
          >
            <Image
              width={1444}
              height={720}
              priority
              src={slide.image}
              alt=""
              className="w-full object-cover"
            />
            <div className="absolute top-20 xl:top-1/4 left-40 w-2/5 text-white">
              <h1 className="text-4xl xl:text-6xl">{slide.title}</h1>
              <p className="text-base my-4">{slide.text}</p>

              <button
                onClick={handleRedirection}
                className="mt-8 px-14 py-2 font-600 text-xl border-white border-2 bg-transparent text-white"
              >
                {slide.button.text}
              </button>
            </div>
          </div>
        ))}
        <button
          type="button"
          onClick={nextSlide}
          className="absolute top-0 right-0 z-30 flex items-center justify-center h-full px-4 cursor-pointer group focus:outline-none"
        >
          <span className="inline-flex items-center justify-center w-8 h-8 rounded-full sm:w-10 sm:h-10 bg-white/30 dark:bg-gray-800/30 group-hover:bg-white/50 dark:group-hover:bg-gray-800/60 group-focus:ring-4 group-focus:ring-white dark:group-focus:ring-gray-800/70 group-focus:outline-none">
            <svg
              aria-hidden="true"
              className="w-5 h-5 text-white sm:w-6 sm:h-6 dark:text-gray-800"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M9 5l7 7-7 7"
              ></path>
            </svg>
            <span className="sr-only">Next</span>
          </span>
        </button>
      </div>
    </div>
  );
}

export default FullWidthCarousel;
