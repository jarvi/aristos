import React, { useContext, useEffect, useState } from 'react';
import UserContext from '../../contexts/UserContext';
import Modal from '../common/Modal';
import Login from './Login';
import ResetPassword from './ResetPassword';
import SignUp from './SignUp';
import { useTranslations } from 'next-intl';
import InformativeModal from '../informativeModal/InformativeModal';



function AuthModal() {
    return (
        <div style={{
            display: 'flex',
            gap: '10px'
        }}>
            <Modal/>
        </div>
    );
}

export default AuthModal;