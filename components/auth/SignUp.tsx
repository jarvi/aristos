import Image from 'next/image';
import React, { FormEvent, useContext, useState } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useForm } from 'react-hook-form';
import AlertContext from '../../contexts/AlertContext';
import UserContext from '../../contexts/UserContext';
import { useTranslations } from "next-intl";
import ModalContext from '../../contexts/ModalContext';

type Props = {
    changeForm: Function
}

const monthsDTO = [
    { name: "Enero", dias: 31 },
    { name: "Febrero", dias: 28 },
    { name: "Marzo", dias: 31 },
    { name: "Abril", dias: 30 },
    { name: "Mayo", dias: 31 },
    { name: "Junio", dias: 30 },
    { name: "Julio", dias: 31 },
    { name: "Agosto", dias: 31 },
    { name: "Septiembre", dias: 30 },
    { name: "Octubre", dias: 31 },
    { name: "Noviembre", dias: 30 },
    { name: "Diciembre", dias: 31 },
]

function SignUp() {
    const t = useTranslations("SignUp");
    const alert = useContext(AlertContext);
    const user = useContext(UserContext);
    const { setShowModal } = useContext(ModalContext)

    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required(t('msg'))
            .min(2),
        lastname: Yup.string()
            .required(t('msg1'))
            .min(2),
        day: Yup.string()
            .required(t('msg2')),
        month: Yup.string()
            .required(t('msg3')),
        year: Yup.string()
            .required(t('msg4')),
        email: Yup.string()
            .required(t('msg5'))
            .email(t('msg6')),
        password: Yup.string()
            .required(t('msg7')),
        privacyPolicy: Yup.boolean()
            .oneOf([true], t('msg8')),
        useTerms: Yup.boolean()
            .oneOf([true], t('msg9')),
        country: Yup.string()
            .required(t('msg10'))
            .oneOf(["El Salvador"], t('msg11') ),
        city: Yup.string()
            .required(t('msg10_1'))
            .oneOf(["San Salvador", "San Miguel"], t('msg11')),
        zipCode: Yup.string()
            .required(t('msg11_1'))
            .min(2),
    });
    const formOptions = { resolver: yupResolver(validationSchema) };

    // get functions to build form with useForm() hook
    const { register, trigger, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    const [step, setStep] = useState(1);

    const todayDate = new Date();

    const days = () => {
        let values = [<option key={`day-0`} disabled value="">{t("msg12")}</option>]
        for (let i = 1; i <= 31; i++) {
            values.push(<option key={`day-${i}`} value={i}>{i}</option>)
        }
        return values;
    }

    const months = () => {
        let values = [<option key={`month-0`} disabled value="">{t("msg13")}</option>]
        for (let i = 0; i < monthsDTO.length; i++) {
            values.push(<option key={`month-${i + 1}`} value={monthsDTO[i].name}>{monthsDTO[i].name}</option>)
        }
        return values;
    }

    const years = () => {
        let values = [<option key={`year-0`} disabled value="">{t("msg14")}</option>]
        for (let i = todayDate.getFullYear(); i >= 1900; i--) {
            values.push(<option key={`year-${i}`} value={i}>{i}</option>)
        }
        return values;
    }

    const getCountries = () => {
        return [
            "El Salvador",
        ]
    }
    const countries = () => {
        const countries = getCountries();
        let values = [<option key={`country`} disabled value="">{t("msg15")}</option>]
        countries.map((country) => {
            values.push(<option key={`country-${country}`} value={country}>{country}</option>)
        })
        return values;
    }

    const cities = () => {
        const getCities = () => {
            return [
                "San Salvador",
                "San Miguel"
            ]
        }
        const cities = getCities();
        let values = [<option key={`city`} disabled value="">{t("msg16")}</option>]
        cities.map((city) => {
            values.push(<option key={`city-${city}`} value={city}>{city}</option>)
        })
        return values;
    }

    const onSubmit = async (data: any) => {
        if (step == 2) {
            const endpoint = '/api/register'

            const JSONdata = JSON.stringify(data)

            const options = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSONdata,
            }

            const response = await fetch(endpoint, options)
            const result = await response.json()
            console.log(result)
            if (result.success) {
                alert.success(result.message)
                console.log(result)
                user.login(result.data)
            } else {
                alert.error(result.message)
            }
        }
    }

    const _handleSubmit = async (e: FormEvent) => {
        e.preventDefault()

        if (step == 1) {
            const isStepOneValid = await trigger([
                "name",
                "lastname",
                "day",
                "month",
                "year",
                "email",
                "password",
                "privacyPolicy",
                "useTerms"
            ]);
            if (isStepOneValid) {
                setStep(step + 1)
            }
        } else {
            handleSubmit(onSubmit)()
        }
    }

    const handleClick = () => {
        //console.log('cerrar el modal de signUp');
        if(step === 2){
            setShowModal(false)
        }
    }

    return (
        <div className='grid grid-cols-5'>
            <div className='col-span-3 relative px-12 py-10'>
                <form className='my-8' onSubmit={_handleSubmit}>
                    {step == 1
                        ?
                        <div className='flex flex-col'>
                            <p className="w-full text-3xl mb-4 text-gray-400">{t("p")}<br />{t("p1")}</p>
                            <p className='text-blue-900 text-xs'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis minima laudantium corrupti nobis aliquid expedita provident mollitia.</p>

                            <div className="w-full mb-8">
                                <input
                                    className={`w-full mt-4 border-b focus:outline-none ${errors.name ? 'border-b-red-700 text-red-700 focus:outline-none focus:shadow-outline' : 'border-b-slate-400 text-slate-400'}`}
                                    type="text"
                                    {...register('name')}
                                    id="name"
                                    placeholder='Primer Nombre'
                                />
                                <div className="text-red-700 text-sm">{errors.name?.message?.toString()}</div>
                            </div>
                            <div className="w-full mb-8">
                                <input
                                    className={`w-full mt-4 border-b focus:outline-none ${errors.lastname ? 'border-b-red-700 text-red-700 focus:outline-none focus:shadow-outline' : 'border-b-slate-400 text-slate-400'}`}
                                    type="text"
                                    {...register('lastname')}
                                    id="lastname"
                                    placeholder='Primer Apellido'
                                />
                                <div className="text-red-700 text-sm">{errors.lastname?.message?.toString()}</div>
                            </div>
                            <p className='mb-4 text-slate-400 text-sm font-semibold'> {t("p2")} </p>
                            <div className='grid grid-cols-3 gap-8 mb-8'>
                                <select
                                    className='border-b border-b-slate-400 text-slate-400 focus:outline-none'
                                    id="day"
                                    {...register('day')}
                                    defaultValue={todayDate.getDate()}
                                >
                                    {days()}
                                </select>
                                <select
                                    className='border-b border-b-slate-400 text-slate-400 focus:outline-none'
                                    id="month"
                                    {...register('month')}
                                    defaultValue={monthsDTO[todayDate.getMonth()].name}
                                >
                                    {months()}
                                </select>
                                <select
                                    className='border-b border-b-slate-400 text-slate-400 focus:outline-none'
                                    id="year"
                                    {...register('year')}
                                    defaultValue={todayDate.getFullYear()}
                                >
                                    {years()}
                                </select>
                            </div>
                            <div className="w-full mb-8">
                                <input
                                    className='w-full border-b border-b-slate-400 text-slate-400 focus:outline-none'
                                    type="email"
                                    id=""
                                    placeholder='Email'
                                    {...register('email')}
                                />
                                <div className="text-red-700 text-sm">{errors.email?.message?.toString()}</div>
                            </div>
                            <div className="w-full mb-8">
                                <input
                                    className='w-full border-b border-b-slate-400 text-slate-400 focus:outline-none'
                                    type="password"
                                    id=""
                                    {...register('password')}
                                />
                                <div className="text-red-700 text-sm">{errors.password?.message?.toString()}</div>
                            </div>
                            <div className="w-full">
                                <p>
                                    <input
                                        type="checkbox"
                                        id="privacyPolicy"
                                        {...register('privacyPolicy')}
                                    />
                                    <label htmlFor='privacyPolicy' className='text-xs ml-4 text-slate-400'> {t("label")} <b> {t("b")}</b></label>
                                </p>
                                <div className="text-red-700 text-sm">{errors.privacyPolicy?.message?.toString()}</div>
                            </div>
                            <div className="w-full">
                                <p>
                                    <input
                                        type="checkbox"
                                        id="useTerms"
                                        {...register('useTerms')}
                                    />
                                    <label htmlFor='useTerms' className='text-xs ml-4 text-slate-400'> {t("label1")}<b> {t("b2")} </b></label>
                                </p>
                                <div className="text-red-700 text-sm">{errors.useTerms?.message?.toString()}</div>
                            </div>
                        </div>
                        :
                        <div className='flex flex-col'>
                            <p className="w-full text-3xl mb-4 text-gray-400">{t("p3")}</p>
                            <p className='text-blue-900 text-xs'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis minima laudantium corrupti nobis aliquid expedita provident mollitia.</p>

                            <div className="w-full mb-8">
                                <select
                                    className='border-b w-full mt-4 border-b-slate-400 text-slate-400 focus:outline-none'
                                    id="country"
                                    {...register('country')}
                                >
                                    {countries()}
                                </select>
                                <div className="text-red-700 text-sm">{errors.country?.message?.toString()}</div>
                            </div>
                            <div className="w-full mb-8">
                                <select
                                    className='border-b w-full border-b-slate-400 text-slate-400 focus:outline-none'
                                    id="city"
                                    {...register('city')}
                                >
                                    {cities()}
                                </select>
                                <div className="text-red-700 text-sm">{errors.city?.message?.toString()}</div>
                            </div>
                            <div className="w-full mb-8">
                                <input
                                    className='w-full mt-4 border-b border-b-slate-400 text-slate-400 focus:outline-none'
                                    type="text"
                                    id="zipCode"
                                    placeholder='Código postal'
                                    {...register('zipCode')}
                                />
                                <div className="text-red-700 text-sm">{errors.zipCode?.message?.toString()}</div>
                            </div>
                        </div>
                    }
                    <div className='flex flex-col'>
                        {step == 2 ?
                            <button  className='col-span-2 w-fit mx-auto px-24 py-2 mt-4 bg-white border border-gray-400 text-gray-400 rounded-lg' name="" id="" onClick={() => setStep(1)}>{t("button")}</button>
                            : ''
                        }
                        <button onClick={handleClick} type='submit' className='col-span-2 w-fit mx-auto px-24 py-2 mt-4 bg-gray-400 border border-gray-400 text-white rounded-lg' name="" id="">{step == 2 ? t("button1") : t("button2") }</button>
                    </div>
                </form>
                <span className="absolute top-4 right-4 text-blue-900 text-xs font-bold leading-none ">
                    {step}/2
                </span>
            </div>
            <div className='col-span-2 relative'>
                <Image fill className='' src="/img/foto2-1.png" alt="" />
            </div>
        </div>
    );
}

export default SignUp;