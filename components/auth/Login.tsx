import Image from "next/image";
import React, { useContext, useEffect, useState } from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useForm } from "react-hook-form";
import AlertContext from "../../contexts/AlertContext";
import UserContext from "../../contexts/UserContext";
import useGlobalUserData from "../../hooks/useGlobalUserData";
import { useTranslations } from "next-intl";
import emulateHttpRequest from "../../helpers/emulateHttpRequest";
import LoadingSpinner from "../spinner/Spinner";
import setLocalStorage from "../../helpers/setLocalStorage";
import ModalContext from "../../contexts/ModalContext";

type Props = {
  changeForm: Function | undefined;
};

const Login = () => {
  const {
    setShowModal,
    setSelectedForm,
  } = useContext(ModalContext);

  const [loading, setLoading] = useState(false);
  const t = useTranslations("Login");
  const alert = useContext(AlertContext);
  //const user = useContext(UserContext);
  const { user, login: setLoginData } = useGlobalUserData();

  useEffect(() => {
    //console.log(user);
  }, [user]);

  useEffect(() => {
    //console.log(alert);
  }, [alert]);

  const validationSchema = Yup.object().shape({
    email: Yup.string().required(t("msg")).email(t("msg1")),
    password: Yup.string().required(t("msg2")),
  });
  const formOptions = { resolver: yupResolver(validationSchema) };

  // get functions to build form with useForm() hook
  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors } = formState;

  const onSubmit = async (data: any) => {
    data = { ...data, includes: ["token"] };

    const JSONdata = JSON.stringify(data);
    const endpoint = "/api/login";
    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSONdata,
    };
    setLoading(true);
    const x = await emulateHttpRequest("");
    const response = await fetch(endpoint, options);
    const result = await response.json();

    setLoading(false);

    if (result.success) {
      alert.success(result.message);
      setLoginData(result.data);
      //console.log(result);

      let filterData = {
        id: result.data.id,
        name: result.data.name,
        email: result.data.email,
        role_id: result.data.role_id,
        country_id: result.data.country_id,
        region_id: result.data.region_id,
        token: result.data.token,
      };
      setLocalStorage("user", filterData);
      setShowModal(false);
    } else {
      alert.error(result.message);
    }
  };

  return (
    <>
      <div className="px-16 py-10 relative z-10 w-full h-full mb-12">
        <div className="flex items-end justify-between mb-24">
          <Image
            width={200}
            height={50}
            className="h-10 w-auto"
            src="/img/logo-horizontal-full-color.png"
            alt="logo"
          />
        </div>
        <div className="mt-6 mb-10 w-full">
          <p className="w-full text-3xl mb-4 text-gray-400">{t("p")}</p>
          <form className="w-full" onSubmit={handleSubmit(onSubmit)}>
            <div className="w-full mb-4">
              <input
                type="email"
                id="email"
                {...register("email")}
                placeholder="E-mail"
                className={`col-span-2 w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400 ${
                  errors.email
                    ? "border border-red-700 focus:outline-none focus:shadow-outline"
                    : ""
                }`}
              />
              <div className="text-red-700 text-sm">
                {errors.email?.message?.toString()}
              </div>
            </div>
            <div className="w-full mb-4">
              <input
                type="password"
                id="password"
                {...register("password")}
                placeholder={t("placeholder")}
                className={`col-span-2 w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400 ${
                  errors.password
                    ? "border border-red-700 focus:outline-none focus:shadow-outline"
                    : ""
                }`}
              />
              <div className="text-red-700 text-sm ">
                {errors.password?.message?.toString()}
              </div>
            </div>
            <button
              type="submit"
              id="login"
              className="col-span-2 w-full h-fit p-2 mb-4 border bg-blue-900 border-gray-400 rounded-md text-white focus:border-gray-400"
            >
              {loading ? (
                <LoadingSpinner />
              ) : alert.alertText ? (
                `${alert.alertText}`
              ) : (
                `${t("enter")}`
              )}
            </button>
          </form>
        </div>
        <div className="flex justify-around mb-8">
          <div>
            <input type="checkbox" name="rememberMe" id="rememberMe" />
            <label htmlFor="rememberMe" className="ml-2 text-sm">
              {t("label")}
            </label>
          </div>
          <p
            className="text-blue-900 text-sm"
            onClick={() => setSelectedForm("resetPasswordForm")}
          >
            {t("p1")}
          </p>
        </div>
        <p
          className="text-blue-900 w-fit mx-auto"
          onClick={() => setSelectedForm("registerForm")}
        >
          {t("p2")}
          <span className="bold">{t("span")}</span>
        </p>
      </div>

      <div className="absolute z-0 bottom-1 w-1/2 left-1 opacity-10">
        <Image
          width={260}
          height={187}
          src="/img/logo-no-text-full-color.png"
          className="w-full h-full"
          alt=""
        />
      </div>
    </>
  );
};

export default Login;
