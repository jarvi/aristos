import Image from 'next/image';
import React, { useContext } from 'react';
import { useTranslations } from "next-intl";
import ModalContext from '../../contexts/ModalContext';

type Props = {
    changeForm: Function;
}

function ResetPassword() {

    //console.log('estas en ResetPassword');
    
    const { setShowModal, setSelectedForm } = useContext(ModalContext);

    const t = useTranslations("ResetPassword");
    
    const handleClickResetPassword =  ()=>{
        //console.log('Cerrar ventana modal');
        setShowModal(false)
    }  

    return (
        <>
            <div className='px-16 py-10 relative z-10 w-full h-full mb-12'>
                <div className="flex items-end justify-between mb-24">
                    <Image width={32} height={32} className="h-10" src="/img/logo-horizontal-full-color.png" alt="logo" />
                </div>
                <div className="mt-6 mb-10 w-full">
                    <p className="w-full text-3xl mb-4 text-gray-400">{t('p')}</p>
                    <input type="email" id='email' placeholder='E-mail' className='col-span-2 w-full h-fit p-2 mb-4 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400' />
                    <p className="w-full text-xs mb-4 text-gray-400">{t('p1')}</p>
                    <input onClick={ handleClickResetPassword } type="button" id='login' value='Entrar' className='col-span-2 w-full h-fit p-2 mb-4 border bg-blue-900 border-gray-400 rounded-md text-white focus:border-gray-400' />
                </div>
                <p className='text-blue-900 w-fit mx-auto' onClick={() => setSelectedForm('registerForm')}>{t('p2')} <span className='bold'>{t('span')}</span></p>
            </div>

            <div className='absolute z-0 bottom-1 left-1 opacity-10'>
                <Image width={32} height={32} src="/img/logo-no-text-full-color.png" className='w-full h-auto' alt="" />
            </div>
        </>
    );
}

export default ResetPassword;