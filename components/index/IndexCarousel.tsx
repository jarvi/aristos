import React, { RefObject } from "react";
import FullWidthCarousel from "../common/FullWidthCarousel";
import { useTranslations } from "next-intl";

function IndexCarousel() {
  const t = useTranslations("Layout");
  const slides = [
    {
      image: "/img/foto3.jpg",
      title: `${t("slides.title")}`,
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum sapiente explicabo impedit commodi libero aliquam eius repudiandae, qui sunt? Molestiae tempora, harum voluptatum eaque qui quaerat natus. Reiciendis, dolores voluptate?",
      button: {
        text: "Nuestros servicios",
        link: "",
      },
    },
    {
      image: "/img/foto3.jpg",
      title: "Asesoramiento Financiero Experto 2",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum sapiente explicabo impedit commodi libero aliquam eius repudiandae, qui sunt? Molestiae tempora, harum voluptatum eaque qui quaerat natus. Reiciendis, dolores voluptate?",
      button: {
        text: "Nuestros servicios",
        link: "",
      },
    },
    {
      image: "/img/foto3.jpg",
      title: "Asesoramiento Financiero Experto 3",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum sapiente explicabo impedit commodi libero aliquam eius repudiandae, qui sunt? Molestiae tempora, harum voluptatum eaque qui quaerat natus. Reiciendis, dolores voluptate?",
      button: {
        text: "Nuestros servicios",
        link: "",
      },
    },
  ];

  return <FullWidthCarousel slides={slides} />;
}

export default IndexCarousel;
