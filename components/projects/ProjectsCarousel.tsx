import React, { RefObject } from 'react';
import FullWidthCarousel from '../common/FullWidthCarousel';

function ProjectsCarousel() {
    const slides = [
        {
            image: "/img/foto3.jpg",
            title: "Proyectos",
            text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum sapiente explicabo impedit commodi libero aliquam eius repudiandae, qui sunt? Molestiae tempora, harum voluptatum eaque qui quaerat natus. Reiciendis, dolores voluptate?",
            button: {
                text: "¡Lanza tu proyecto!",
                link: "/projects/new"
            }
        },
        {
            image: "/img/foto3.jpg",
            title: "Proyectos 2",
            text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum sapiente explicabo impedit commodi libero aliquam eius repudiandae, qui sunt? Molestiae tempora, harum voluptatum eaque qui quaerat natus. Reiciendis, dolores voluptate?",
            button: {
                text: "¡Lanza tu proyecto!",
                link: "/projects/new"
            }
        },
        {
            image: "/img/foto3.jpg",
            title: "Proyectos 3",
            text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum sapiente explicabo impedit commodi libero aliquam eius repudiandae, qui sunt? Molestiae tempora, harum voluptatum eaque qui quaerat natus. Reiciendis, dolores voluptate?",
            button: {
                text: "¡Lanza tu proyecto!",
                link: "/projects/new"
            }
        },
    ]

    return (
        <>
            <FullWidthCarousel slides={slides} />
        </>
    );
}

export default ProjectsCarousel;