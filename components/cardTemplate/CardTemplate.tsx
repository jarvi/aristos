import DownloadTemplates from "../downloadTemplates/DownloadTemplates";
import React, { useEffect, useState } from "react";
import {
  faCircle,
  faFileExcel,
  faFileWord,
} from "@fortawesome/free-solid-svg-icons";

export default function CardTemplate(props: any) {
  const { arguments: arg } = props;

  return (
    <div className="w-40 xl:min-w-[19%] flex border-2 border-t-8 p-2 mb-6 rounded-lg border-gray-200 border-t-blue-900">
      <div className="w-full flex flex-col justify-center items-center">
        <p className="text-xl">{arg.p}</p>
        <p>{arg.p1}</p>
      </div>

      <div>
        <DownloadTemplates
          id={arg.word}
          icon={faFileWord}
          styleClass={"w-8 h-8 mb-2 text-blue-700"}
        />
        <DownloadTemplates
          id={arg.excel}
          icon={faFileExcel}
          styleClass={"plantilla w-8 h-8 text-green-500"}
        />
      </div>
    </div>
  );
}
