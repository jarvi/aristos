import React, { useContext, useEffect } from "react";
import Image from "next/image";
import useGlobalUserData from "../../hooks/useGlobalUserData";
import ModalContext from "../../contexts/ModalContext";

type Props = {
  changeForm: Function;
};

export default function InformativeModal() {
  const { userStatus, setUserStatus } = useGlobalUserData();
  const {
    showModal: modalState,
    setShowModal,
    selectedForm,
    setSelectedForm,
  } = useContext(ModalContext);

  const closeSession = () => {
    console.log("en el modal nuevo");

    window.localStorage.clear();
    setUserStatus(false);
    setShowModal(false)
    //changeForm('informativeModal');
  };

  // useEffect(() => {
  //   console.log("cambiado el modal a informative");
  //   setSelectedForm("informativeModal");
  // }, []);

  return (
    <>
      <div className="px-16 py-10 relative z-10 w-full h-full mb-12">
        <div className="flex items-end justify-between mb-24">
          <Image
            width={200}
            height={50}
            className="h-10 w-auto"
            src="/img/logo-horizontal-full-color.png"
            alt="logo"
          />
        </div>
        <div className="mt-6 mb-10 w-full">
          <p className="w-full text-3xl mb-4 text-gray-400">
            {"Presione aceptar para cerrar la session" /* t("p")*/}
          </p>
        </div>
        <div className="flex justify-around mb-8">
          <div>
            {/* <input type="checkbox" name="rememberMe" id="rememberMe" /> */}
            <label htmlFor="rememberMe" className="ml-2 text-sm">
              {/* {t("label")} */}
            </label>
          </div>
        </div>

        <button
          //type="submit"
          onClick={closeSession}
          id="informative"
          className="col-span-2 w-full h-fit p-2 mb-4 border bg-blue-900 border-gray-400 rounded-md text-white focus:border-gray-400"
        >
          Aceptar
        </button>
      </div>

      <div className="absolute z-0 bottom-1 w-1/2 left-1 opacity-10">
        <Image
          width={260}
          height={187}
          src="/img/logo-no-text-full-color.png"
          className="w-full h-full"
          alt=""
        />
      </div>
    </>
  );
}
