import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleDown,
  faAngleUp,
  faFileExcel,
  faFileWord,
} from "@fortawesome/free-solid-svg-icons";
import { DocumentsGenerals } from "../../util/constants";
import { useTranslations } from "next-intl";
import CreateInputFile from "../../helpers/CreateInputFile";

import UploadFile from "../uploadFile/IconUploadFile";
import CheckedFile from "../checkedFile/CheckedFile";
import InvalidFile from "../invalidFile/InvalidFile";
function UploadDocumentGeneral() {
  const t = useTranslations("fundingPage");
  const [isGeneralDocumentsHidden, setIsGeneralDocumentsHidden] =
    useState(true);
  const [LoadDUI, setLoadDUI] = useState("upload");
  const [LoadTax_ID, setLoadTax_ID] = useState("upload");
  const [LoadRUC, setLoadRUC] = useState("upload");
  const [LoadConstitution_deed, setLoadConstitution_deed] = useState("upload");
  const [LoadBoard_credentials, setLoadBoard_credentials] = useState("upload");
  const [LoadGeneral_power, setLoadGeneral_power] = useState("upload");
  const [LoadFinancial_statements, setLoadFinancial_statements] =
    useState("upload");
  const [LoadVat_declarations, setLoadVat_declarations] = useState("upload");
  const [LoadIncome_statements, setLoadIncome_statements] = useState("upload");

  const [
    requiredQuantityGeneralDocuments,
    setRequiredQuantityGeneralDocuments,
  ] = useState<boolean>(false);

  // estado que almacena los documentos generales
  const [objDocumentsGenerals, setObjDocumentsGenerals] = useState({});

  const fileStates: any = {
    undefined: "upload",
    true: "valid",
    false: "invalid",
  };

  const validLengthGeneralDocuments: number = 9;
  const validateNumberGeneralDocuments = () => {
    const totalQuantity = Object.keys(objDocumentsGenerals).length;

    setRequiredQuantityGeneralDocuments(
      totalQuantity === validLengthGeneralDocuments
    );
  };

  useEffect(() => {
    validateNumberGeneralDocuments();

    //console.log(objDocumentsGenerals);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [objDocumentsGenerals]);

  const generalDocumentFunctions: any = {
    DUI: setLoadDUI,
    Tax_ID: setLoadTax_ID,
    RUC: setLoadRUC,
    Constitution_deed: setLoadConstitution_deed,
    Board_credentials: setLoadBoard_credentials,
    General_power: setLoadGeneral_power,
    Financial_statements: setLoadFinancial_statements,
    Vat_declarations: setLoadVat_declarations,
    Income_statements: setLoadIncome_statements,
  };

  const handleGeneralDocument = (
    inputId: string,
    file: any,
    fileState: string
  ) => {
    if (fileState) {
      setObjDocumentsGenerals((x) => ({
        ...x,
        [DocumentsGenerals[inputId]]: file,
      }));
    }

    if (!fileState) {
      setObjDocumentsGenerals((x: any) => {
        const { [inputId]: deleted, ...rest } = x;
        return rest;
      });
    }

    generalDocumentFunctions[inputId]?.(fileStates[fileState]);
  };

  const selectDocFunction: any = {
    general: handleGeneralDocument,
  };

  const handleFileChange = (e: any, type: string) => {
    const file = e.target.files[0];
    const FileState = /.\b(xls|xlsx|doc|docx)\b/.test(file.name);
    const inputId = e.currentTarget.id;
    selectDocFunction[type]?.(inputId, file, FileState);
  };

  const uploadGeneralDocument = (e: any) => {
    //console.log(e.currentTarget.id);
    const liId = e.currentTarget.id;
    const inputFile = CreateInputFile(liId);
    inputFile.addEventListener("change", (e) => handleFileChange(e, "general"));
  };

  const handleFileState = (fileState: any) => {
    //console.log(fileState);
    const fileStates: any = {
      upload: <UploadFile />,
      valid: <CheckedFile />,
      invalid: <InvalidFile />,
    };

    return fileStates[fileState];
  };

  return (
    <div className="w-72 xl:min-w-[45%] flex flex-col border-2 border-t-8 p-2 mr-4 rounded-lg border-gray-200 border-t-blue-900">
      <div className="w-full flex">
        <div className="w-full flex flex-col justify-center items-center">
          <p>{t("p30")}</p>
          <p className="text-2xl">{t("p31")}</p>
        </div>
        <div>
          <FontAwesomeIcon
            className="w-8 h-8 mb-2 text-blue-700"
            icon={faFileWord}
          />
          <FontAwesomeIcon
            className="w-8 h-8 text-green-500"
            icon={faFileExcel}
          />
        </div>
      </div>
      <div
        className={`flex flex-col justify-start items-center text-start ${
          isGeneralDocumentsHidden ? "hidden" : ""
        }`}
      >
        <ul className="text-sm flex flex-col px-2">
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadGeneralDocument(e)}
            id={`${[DocumentsGenerals.DUI]}`}
          >
            {t("ul.li")}
            {handleFileState(LoadDUI)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadGeneralDocument(e)}
            id={`${[DocumentsGenerals.Tax_ID]}`}
          >
            {t("ul.li_1")}
            {handleFileState(LoadTax_ID)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadGeneralDocument(e)}
            id={`${[DocumentsGenerals.RUC]}`}
          >
            {t("ul.li_2")}
            {handleFileState(LoadRUC)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadGeneralDocument(e)}
            id={`${[DocumentsGenerals.Constitution_deed]}`}
          >
            {t("ul.li_3")}
            {handleFileState(LoadConstitution_deed)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadGeneralDocument(e)}
            id={`${[DocumentsGenerals.Board_credentials]}`}
          >
            {t("ul.li_4")}
            {handleFileState(LoadBoard_credentials)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadGeneralDocument(e)}
            id={`${[DocumentsGenerals.General_power]}`}
          >
            {t("ul.li_5")}
            {handleFileState(LoadGeneral_power)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadGeneralDocument(e)}
            id={`${[DocumentsGenerals.Financial_statements]}`}
          >
            {t("ul.li_6")}
            {handleFileState(LoadFinancial_statements)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadGeneralDocument(e)}
            id={`${[DocumentsGenerals.Vat_declarations]}`}
          >
            {t("ul.li_7")}
            {handleFileState(LoadVat_declarations)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadGeneralDocument(e)}
            id={`${[DocumentsGenerals.Income_statements]}`}
          >
            {t("ul.li_8")}
            {handleFileState(LoadIncome_statements)}
          </li>
        </ul>

        <button
          className={
            requiredQuantityGeneralDocuments
              ? "col-span-1 w-fit justify-self-center px-12 py-1  text-white rounded-lg bg-blue-900"
              : "col-span-1 w-fit justify-self-center px-12 py-1  text-white rounded-lg bg-gray-400 "
          }
          type="submit"
          onClick={() => console.log("enviar al server")}
        >
          Enviar
        </button>
      </div>
      <button
        className="border-none mx-auto"
        onClick={() => setIsGeneralDocumentsHidden(!isGeneralDocumentsHidden)}
      >
        <FontAwesomeIcon
          className="w-5 h-5"
          icon={isGeneralDocumentsHidden ? faAngleDown : faAngleUp}
        />
      </button>
    </div>
  );
}

export default UploadDocumentGeneral;
