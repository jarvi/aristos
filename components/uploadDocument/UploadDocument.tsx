import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleDown,
  faAngleUp,
  faFileExcel,
  faFileWord,
} from "@fortawesome/free-solid-svg-icons";
import { useTranslations } from "next-intl";
import CreateInputFile from "../../helpers/CreateInputFile";

import UploadFile from "../uploadFile/IconUploadFile";
import CheckedFile from "../checkedFile/CheckedFile";
import InvalidFile from "../invalidFile/InvalidFile";

export default function UploadDocument(props: any) {
  const { documents, lengthValid, title } = props;
  //console.log(props);
  const [UploadDocument, setUploadDocument] = useState<any>({});
  const [objdocuments, setObjdocuments] = useState({});

  const [isRepresentativeDocumentsHidden, setIsRepresentativeDocumentsHidden] =
    useState(true);

  const [
    requiredQuantityRepresentativeDocuments,
    setRequiredQuantityRepresentativeDocuments,
  ] = useState<boolean>(false);

  useEffect(() => {
    setUploadDocument(() => {
      const loadedObject = documents.reduce((obj: any, value: any) => {
        obj[value] = "upload";
        return obj;
      }, {});
      return loadedObject;
    });
  }, [documents]);

  //useEffect(() => {}, [UploadDocument]);

  const fileStates: any = {
    undefined: "upload",
    true: "valid",
    false: "invalid",
  };

  const validateNumberRepresentativeDocuments = () => {
    const totalQuantity = Object.keys(objdocuments).length;
    setRequiredQuantityRepresentativeDocuments(totalQuantity === lengthValid);
  };

  const handleRepresentativeDocument = (
    inputId: string,
    file: any,
    fileState: string
  ) => {
    if (fileState) {
      setObjdocuments((x) => ({
        ...x,
        [inputId]: file,
      }));
    }

    if (!fileState) {
      setObjdocuments((x: any) => {
        const { [inputId]: deleted, ...rest } = x;
        return rest;
      });
    }

    setUploadDocument((x: any) => {
      const currentDocuments = { ...x, [inputId]: fileStates[fileState] };
      return currentDocuments;
    });
  };

  const selectDocFunction: any = {
    representative: handleRepresentativeDocument,
  };

  const handleFileChange = (e: any, type: string) => {
    const file = e.target.files[0];
    const FileState = /.\b(xls|xlsx|doc|docx)\b/.test(file.name);
    const inputId = e.currentTarget.id;
    selectDocFunction[type]?.(inputId, file, FileState);
  };

  const uploadRepresentativeDocument = (e: any) => {
    const liId = e.currentTarget.id;
    const inputFile = CreateInputFile(liId);
    inputFile.addEventListener("change", (e) =>
      handleFileChange(e, "representative")
    );
  };

  const handleFileState = (fileState: any) => {
    const fileStates: any = {
      upload: <UploadFile />,
      valid: <CheckedFile />,
      invalid: <InvalidFile />,
    };

    return fileStates[fileState];
  };

  useEffect(() => {
    validateNumberRepresentativeDocuments();
    ////console.log(objdocuments);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [objdocuments]);

  // useEffect(() => {
  //   return () => {
  //     console.log('limpiando');
      
  //     setObjdocuments({});
  //   };
  // },[]);

  return (
    <div className="w-72 xl:min-w-[45%] flex flex-col border-2 border-t-8 p-2 rounded-lg border-gray-200 border-t-blue-900">
      <div className="w-full flex">
        <div className="w-full flex flex-col justify-center items-center">
          <p>{title.p}</p>
          <p className="text-2xl">{title.p1}</p>
        </div>
        <div>
          <FontAwesomeIcon
            className="w-8 h-8 mb-2 text-blue-700"
            icon={faFileWord}
          />
          <FontAwesomeIcon
            className="w-8 h-8 text-green-500"
            icon={faFileExcel}
          />
        </div>
      </div>
      <div
        className={`flex flex-col justify-start items-center text-start ${
          isRepresentativeDocumentsHidden ? "hidden" : ""
        }`}
      >
        <ul className="text-sm flex flex-col px-2 w-full">
          {documents.map((v: any, i: any) => (
            <li
              key={i}
              className="cursor-pointer flex items-center justify-between"
              onClick={(e) => uploadRepresentativeDocument(e)}
              id={`${v}`}
            >
              {`${v}`}

              {handleFileState(UploadDocument[v])}
            </li>
          ))}
        </ul>
        <button
          className={
            requiredQuantityRepresentativeDocuments
              ? "col-span-1 w-fit justify-self-center px-12 py-1  text-white rounded-lg bg-blue-900"
              : "col-span-1 w-fit justify-self-center px-12 py-1  text-white rounded-lg bg-gray-400 "
          }
          type="submit"
          onClick={() => {
            console.log("enviar documentos al server", title.p, title.p1);
            console.log(objdocuments);
          }}
          disabled={!requiredQuantityRepresentativeDocuments}
        >
          Enviar
        </button>
      </div>
      <button
        className="border-none mx-auto"
        onClick={() =>
          setIsRepresentativeDocumentsHidden(!isRepresentativeDocumentsHidden)
        }
      >
        <FontAwesomeIcon
          className="w-5 h-5"
          icon={isRepresentativeDocumentsHidden ? faAngleDown : faAngleUp}
        />
      </button>
    </div>
  );
}
