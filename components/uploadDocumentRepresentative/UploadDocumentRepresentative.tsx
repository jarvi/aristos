import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleDown,
  faAngleUp,
  faFileExcel,
  faFileWord,
} from "@fortawesome/free-solid-svg-icons";
import { DocumentsRepresentative } from "../../util/constants";
import { useTranslations } from "next-intl";
import CreateInputFile from "../../helpers/CreateInputFile";

import UploadFile from "../uploadFile/IconUploadFile";
import CheckedFile from "../checkedFile/CheckedFile";
import InvalidFile from "../invalidFile/InvalidFile";

export default function UploadDocumentRepresentative() {
  const t = useTranslations("fundingPage");

  const [isRepresentativeDocumentsHidden, setIsRepresentativeDocumentsHidden] =
    useState(true);

  const [LoadDUIRepresentative, setLoadDUIRepresentative] = useState("upload");
  const [LoadNITRepresentative, setLoadNITRepresentative] = useState("upload");
  const [LoadPassportRepresentative, setLoadPassportRepresentative] =
    useState("upload");

  const [LoadResident_CardRepresentative, setLoadResident_CardRepresentative] =
    useState("upload");

  const [
    requiredQuantityRepresentativeDocuments,
    setRequiredQuantityRepresentativeDocuments,
  ] = useState<boolean>(false);

  const [objDocumentsRepresentative, setObjDocumentsRepresentative] = useState(
    {}
  );

  const fileStates: any = {
    undefined: "upload",
    true: "valid",
    false: "invalid",
  };

  const validLengthRepresentativeDocuments: number = 4;

  const validateNumberRepresentativeDocuments = () => {
    const totalQuantity = Object.keys(objDocumentsRepresentative).length;
    setRequiredQuantityRepresentativeDocuments(
      totalQuantity === validLengthRepresentativeDocuments
    );
  };

  const representativeDocumentFunctions: any = {
    DUI: setLoadDUIRepresentative,
    NIT: setLoadNITRepresentative,
    Passport: setLoadPassportRepresentative,
    Resident_Card: setLoadResident_CardRepresentative,
  };

  const handleRepresentativeDocument = (
    inputId: string,
    file: any,
    fileState: string
  ) => {
    if (fileState) {
      setObjDocumentsRepresentative((x) => ({
        ...x,
        [DocumentsRepresentative[inputId]]: file,
      }));
    }

    if (!fileState) {
      setObjDocumentsRepresentative((x: any) => {
        const { [inputId]: deleted, ...rest } = x;
        return rest;
      });
    }

    representativeDocumentFunctions[inputId]?.(fileStates[fileState]);
  };

  const selectDocFunction: any = {
    representative: handleRepresentativeDocument,
  };

  const handleFileChange = (e: any, type: string) => {
    const file = e.target.files[0];
    const FileState = /.\b(xls|xlsx|doc|docx)\b/.test(file.name);
    const inputId = e.currentTarget.id;
    selectDocFunction[type]?.(inputId, file, FileState);
  };

  const uploadRepresentativeDocument = (e: any) => {
    const liId = e.currentTarget.id;
    const inputFile = CreateInputFile(liId);
    inputFile.addEventListener("change", (e) =>
      handleFileChange(e, "representative")
    );
  };

  const handleFileState = (fileState: any) => {
    const fileStates: any = {
      upload: <UploadFile />,
      valid: <CheckedFile />,
      invalid: <InvalidFile />,
    };

    return fileStates[fileState];
  };

  useEffect(() => {
    validateNumberRepresentativeDocuments();
    //console.log(objDocumentsRepresentative);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [objDocumentsRepresentative]);
  return (
    <div className="w-72 xl:min-w-[45%] flex flex-col border-2 border-t-8 p-2 rounded-lg border-gray-200 border-t-blue-900">
      <div className="w-full flex">
        <div className="w-full flex flex-col justify-center items-center">
          <p>{t("p32")}</p>
          <p className="text-2xl">{t("p33")}</p>
        </div>
        <div>
          <FontAwesomeIcon
            className="w-8 h-8 mb-2 text-blue-700"
            icon={faFileWord}
          />
          <FontAwesomeIcon
            className="w-8 h-8 text-green-500"
            icon={faFileExcel}
          />
        </div>
      </div>
      <div
        className={`flex flex-col justify-start items-center text-start ${
          isRepresentativeDocumentsHidden ? "hidden" : ""
        }`}
      >
        <ul className="text-sm flex flex-col px-2 w-full">
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadRepresentativeDocument(e)}
            id={`${[DocumentsRepresentative.DUI]}`}
          >
            {t("ul_1.li")}
            {handleFileState(LoadDUIRepresentative)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadRepresentativeDocument(e)}
            id={`${[DocumentsRepresentative.NIT]}`}
          >
            {t("ul_1.li_1")}
            {handleFileState(LoadNITRepresentative)}
          </li>

          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadRepresentativeDocument(e)}
            id={`${[DocumentsRepresentative.Passport]}`}
          >
            {t("ul_1.li_2")}
            {handleFileState(LoadPassportRepresentative)}
          </li>
          <li
            className="cursor-pointer flex items-center justify-between"
            onClick={(e) => uploadRepresentativeDocument(e)}
            id={`${[DocumentsRepresentative.Resident_Card]}`}
          >
            {t("ul_1.li_3")}
            {handleFileState(LoadResident_CardRepresentative)}
          </li>
        </ul>
        <button
          className={
            requiredQuantityRepresentativeDocuments
              ? "col-span-1 w-fit justify-self-center px-12 py-1  text-white rounded-lg bg-blue-900"
              : "col-span-1 w-fit justify-self-center px-12 py-1  text-white rounded-lg bg-gray-400 "
          }
          type="submit"
          onClick={() => {
            console.log("enviar documentos al server");
            console.log(objDocumentsRepresentative);
          }}
        >
          Enviar
        </button>
      </div>
      <button
        className="border-none mx-auto"
        onClick={() =>
          setIsRepresentativeDocumentsHidden(!isRepresentativeDocumentsHidden)
        }
      >
        <FontAwesomeIcon
          className="w-5 h-5"
          icon={isRepresentativeDocumentsHidden ? faAngleDown : faAngleUp}
        />
      </button>
    </div>
  );
}
