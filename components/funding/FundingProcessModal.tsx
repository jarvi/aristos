import Image from 'next/image';
import React from 'react';
import Modal from '../common/Modal';
import { useTranslations } from 'next-intl'

type Props = {
    children: JSX.Element,
    text: string
};

function FundingProcessModal({ children, text }: Props) {
    const t = useTranslations('FundingProcessModal');
    return (
        <div>
            <Modal
                buttonType="px-8 py-1 mt-4 rounded-md font-light text-lg border-blue-900 bg-blue-900 text-white"
                text={`${t('text')}`}
            >
                <div className='px-16 py-10 relative w-full h-full'>
                    <div className='z-10 w-full h-full relative'>
                        <div className="flex items-end justify-between">
                            <Image width={220} height={50} className="h-10 w-auto" src="/img/logo-horizontal-full-color.png" alt="logo" />
                            <span className="text-sm text-gray-400">{text}</span>
                        </div>
                        <div className="mt-6 mb-10 w-full">
                            <p className="w-full border-b border-solid border-slate-200 text-3xl text-gray-400">{t('p')}</p>
                            <p className="text-xs text-blue-900">{t('p1')}</p>
                        </div>
                        {children}
                        <div className="flex justify-evenly align-middle mt-6 p-6 mb-10 w-full border border-solid border-slate-200">
                            <div className='w-3/5'>
                                <p className="text-2xl text-gray-400">{t('p3')}</p>
                                <p className="text-base text-gray-400">{t('p4')}</p>
                            </div>
                            <p className="w-2/5 text-xs text-blue-900">{t('p5')}</p>
                        </div>
                    </div>
                    <div className='absolute z-0 bottom-1 left-1 opacity-10 w-1/2'>
                        <Image width={260} height={187} src="/img/logo-no-text-full-color.png" className='w-full h-auto' alt="" />
                    </div>
                </div>
            </Modal>
        </div>
    );
}

export default FundingProcessModal;