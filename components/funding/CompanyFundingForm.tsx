import React, { useEffect, useState } from "react";
import FundingProcessModal from "./FundingProcessModal";
import { useTranslations } from "next-intl";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import LoadingSpinner from "../spinner/Spinner";
import emulateHttpRequest from "../../helpers/emulateHttpRequest";

export default function CompanyFundingForm() {
  const [loading, setLoading] = useState(false);
  const t = useTranslations("CompanyFundingForm");
  const schema = Yup.object({
    fullName: Yup.string().required(t("msg")),
    address: Yup.string().required(t("msg")),
    contactName: Yup.string().required(t("msg")),
    email: Yup.string().required(t("msg")).email(t("msg2")),
    phoneNumber: Yup.string().required(t("msg")).min(8, t("msg1")),
  }).required();

  const {
    setError,
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data: any) => {
    console.log(data);
    setLoading(true);
    const serverRequest = await emulateHttpRequest("");
    setLoading(false);
  };

  useEffect(() => {
    console.log(errors);
  }, [errors]);

  return (
    <div>
      <FundingProcessModal text={t("Companies")}>
        <form
          onSubmit={handleSubmit(onSubmit)}
          className="grid grid-cols-2 gap-12 grid-rows-4"
        >
          <input
            {...register("fullName")}
            className={`border-b border-b-slate-400 text-slate-400 focus:outline-none ${
              errors.fullName
                ? "border border-red-700 focus:outline-none focus:shadow-outline"
                : ""
            }`}
            type="text"
            id=""
            placeholder={t("FullName")}
          />
          <input
            {...register("address")}
            className={`border-b border-b-slate-400 text-slate-400 focus:outline-none ${
              errors.address
                ? "border border-red-700 focus:outline-none focus:shadow-outline"
                : ""
            }`}
            type="text"
            id=""
            placeholder={t("Direction")}
          />
          <input
            {...register("contactName")}
            className={`border-b border-b-slate-400 text-slate-400 focus:outline-none ${
              errors.contactName
                ? "border border-red-700 focus:outline-none focus:shadow-outline"
                : ""
            }`}
            type="text"
            id=""
            placeholder={t("ContactName")}
          />
          <input
            {...register("email")}
            className={`border-b border-b-slate-400 text-slate-400 focus:outline-none ${
              errors.email
                ? "border border-red-700 focus:outline-none focus:shadow-outline"
                : ""
            }`}
            type="email"
            id=""
            placeholder={t("Email")}
          />
          <input
            {...register("phoneNumber")}
            className={`border-b border-b-slate-400 text-slate-400 focus:outline-none ${
              errors.phoneNumber
                ? "border border-red-700 focus:outline-none focus:shadow-outline"
                : ""
            }`}
            type="text"
            id=""
            placeholder={t("PhoneNumber")}
          />
          <p>No soy un robot</p>
          <button
            className="col-span-2 w-fit justify-self-center px-12 py-1 bg-gray-400 text-white rounded-lg"
            type="submit"
          >
            {loading ? <LoadingSpinner /> : `${t("Send")}`}
          </button>
        </form>
      </FundingProcessModal>
    </div>
  );
}
