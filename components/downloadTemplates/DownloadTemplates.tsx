import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import DownloadDoc from "../../helpers/downloadDoc";

export default function DownloadTemplates(props: any) {
  const { id, icon, styleClass } = props;
//   console.log(id);
//   console.log(icon);

  return (
    <FontAwesomeIcon
      style={{ cursor: "pointer" }}
      className={styleClass}
      id={id}
      icon={icon}
      onClick={(e) => DownloadDoc(e)}
    />
  );
}
