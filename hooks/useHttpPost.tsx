import { useEffect, useState } from "react";
import useGlobalUserData from "./useGlobalUserData";

export default function useHttpPost(url: string = "", data: any = "") {
  const [response, setResponse] = useState<any>(null);
  const [error, setError] = useState<any>(null);
  const { setUser } = useGlobalUserData();

  useEffect(() => {
    if (url && data) {
      console.log("estas dentro del useHttpPost");
      const fetchData = async () => {
        try {
          const response = await fetch(url, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
          });

          const responseBody = await response.json();
          const requestStatus = response.status;

          if (requestStatus === 401) {
            console.log("el token caduco");
            window.localStorage.clear();
            setUser(null);

            //mandar a home 
          }

          setResponse({ responseBody, requestStatus });
        } catch (error) {
          setError(error);
        }
      };
      fetchData();
    }
  }, [url, data, setUser]);

  return { response, error };
}
