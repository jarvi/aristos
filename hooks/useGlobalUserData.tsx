/* eslint-disable import/no-anonymous-default-export */
import { useContext } from "react";
import UserContext from "../contexts/UserContext";
export default function ()  {
  return useContext(UserContext);
}


