/** @type {import('tailwindcss').Config} */

const defaultTheme = require("tailwindcss/defaultTheme")

module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        "lato": ["lato", ...defaultTheme.fontFamily.sans],
        "revalia": ["revalia", ...defaultTheme.fontFamily.sans],
        "helios-antique": ["helios-antique", ...defaultTheme.fontFamily.sans],
      },
    },
  },
  plugins: [],
}
