import "../styles/globals.css";
import Layout from "../components/common/Layout";
import type { AppProps } from "next/app";
import { NextIntlProvider } from "next-intl";
import { AlertProvider } from "../contexts/AlertContext";
import { UserProvider } from "../contexts/UserContext";
import { ModalProvider } from "../contexts/ModalContext";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <NextIntlProvider messages={pageProps.messages}>
      <AlertProvider>
        <UserProvider>
          <ModalProvider>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </ModalProvider>
        </UserProvider>
      </AlertProvider>
    </NextIntlProvider>
  );
}
