import React from "react";

import { Inter } from "@next/font/google";
import Image from "next/image";
import IndexCarousel from "../components/index/IndexCarousel";
import { GetStaticPropsContext } from "next";
import { useTranslations } from "next-intl";
import Video from "../components/common/Video";

const inter = Inter({ subsets: ["latin"] });

export default function Us() {
  const t = useTranslations("AboutUsPage");
  return (
    <>
      <div className="w-full flex justify-center relative h-4/5 flex-col">
        <Video />
        <div className=" flex justify-end  boton-chat">
          <p>Habla con nosotros</p>
          <svg
            className="h-12 w-12 text-black"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M17 8h2a2 2 0 012 2v6a2 2 0 01-2 2h-2v4l-4-4H9a1.994 1.994 0 01-1.414-.586m0 0L11 14h4a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2v4l.586-.586z"
            />
          </svg>
        </div>
      </div>

      <div className="w-full px-12 mt-20">
        <div className="flex flex-col items-center w-11/12 mx-auto">
          <h1 className="text-4xl font-semibold pb-4 w-auto font-revalia text-blue-900 border-b-2 border-blue-900">
            Aristos
          </h1>
          <div className="grid grid-cols-2 px-12 gap-4 mt-20">
            <div className="relative h-fit">
              <div className="absolute z-0 w-full h-full m-auto opacity-10 flex justify-center items-center">
                <Image
                  width={250}
                  height={250}
                  src="/img/logo-no-text-full-color.png"
                  className="w-full h-auto"
                  alt=""
                />
              </div>
              <div className="grid grid-cols-2 gap-2">
                <Image
                  width={250}
                  height={250}
                  src="/img/foto2-1.png"
                  className="z-10 w-56 h-auto object-cover"
                  alt=""
                />
                <Image
                  width={400}
                  height={560}
                  src="/img/foto2-2.png"
                  className="z-10 w-56 h-auto object-cover mt-28 xl:mt-40"
                  alt=""
                />
              </div>
            </div>
            <div className="col-span-1 font-helios-antique">
              <h1 className="text-gray-400 text-base">
                <span className="text-2xl">{t("h1.span")}</span>
                <br />
                {t("h1.h1")}
              </h1>
              <p className="text-blue-900 text-xs mt-4">{t("p")}</p>

              <h2 className="text-gray-400 text-2xl mt-6">{t("h2")}</h2>
              <p className="text-blue-900 text-xs mt-4">{t("p1")}</p>
              <p className="text-blue-900 text-xs mt-2">{t("p2")}</p>
              <p className="text-blue-900 text-xs mt-2">{t("p3")}</p>
              <p className="text-blue-900 text-xs mt-2">{t("p4")}</p>

              <h2 className="text-gray-400 text-2xl mt-6">{t("p5")}</h2>
              <p className="text-blue-900 text-xs mt-4">{t("p6")}</p>
              <p className="text-blue-900 text-xs mt-2">{t("p7")}</p>
            </div>
          </div>
        </div>
      </div>

      <div className="w-full px-12 mt-20">
        <div className="flex flex-col items-center w-11/12 mx-auto">
          <div className="grid grid-cols-2 px-12 gap-4 mt-20">
            <p className="text-blue-900 text-lg mt-4">
              <strong> Aristos </strong> {t("p8")}
            </p>

            <div className="img-content">
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                }}
                className="img-group "
              >
                <div className="column-one">
                  <Image
                    width={150}
                    height={150}
                    src="/img/foto2-1.png"
                    className=""
                    alt=""
                  />
                  <Image
                    width={150}
                    height={150}
                    src="/img/foto2-2.png"
                    className=""
                    alt=""
                  />
                </div>

                <div className="column-two">
                  <Image
                    width={150}
                    height={150}
                    src="/img/foto2-1.png"
                    className=""
                    alt=""
                  />
                  <Image
                    width={150}
                    height={150}
                    src="/img/foto2-2.png"
                    className=""
                    alt=""
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="w-full px-12">
        <div className="flex flex-col items-center w-11/12 mx-auto">
          <div className="grid grid-cols-2 px-12 gap-4 mt-20">
            <div className="img-content">
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-start",
                }}
                className="img-group "
              >
                <div className="column-one">
                  <Image
                    width={150}
                    height={150}
                    src="/img/foto2-1.png"
                    className=""
                    alt=""
                  />
                  <Image
                    width={150}
                    height={150}
                    src="/img/foto2-2.png"
                    className=""
                    alt=""
                  />
                </div>

                <div className="column-two">
                  <Image
                    width={150}
                    height={150}
                    src="/img/foto2-1.png"
                    className=""
                    alt=""
                  />
                  <Image
                    width={150}
                    height={150}
                    src="/img/foto2-2.png"
                    className=""
                    alt=""
                  />
                </div>
              </div>
            </div>
            <p className="text-blue-900 text-lg mt-4">
              <strong> {t("strong")} </strong>
              {t("p9")}
              <strong> {t("strong_1")} </strong> {t("p10")}
            </p>
          </div>
        </div>
      </div>

      <div className="w-full px-12">
        <strong> Como funciona </strong>

        <div
          style={{
            display: "flex",
            justifyContent: "center",
          }}
          className="img-content"
        >
          <Image
            width={600}
            height={200}
            src="/img/foto2-2.png"
            className=""
            alt=""
          />
        </div>
      </div>

      <div className="w-full px-12">
        <strong> Nuestro equipo </strong>

        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
          }}
          className="img-content"
        >
          <div className="img-content-team ">
            <Image
              width={200}
              height={200}
              src="/img/foto2-2.png"
              className=""
              alt=""
            />
            <strong>CREATIVO</strong>
          </div>
          <div className="img-content-team ">
            <Image
              width={200}
              height={200}
              src="/img/foto2-2.png"
              className=""
              alt=""
            />
            <strong>ADVISOR</strong>
          </div>
          <div className="img-content-team ">
            <Image
              width={200}
              height={200}
              src="/img/foto2-2.png"
              className=""
              alt=""
            />
            <strong>CFO</strong>
          </div>
          <div className="img-content-team ">
            <Image
              width={200}
              height={200}
              src="/img/foto2-2.png"
              className=""
              alt=""
            />
            <strong>CEO</strong>
          </div>
        </div>
      </div>
      <div className="w-full bg-[url('/img/foto1.jpg')] bg-cover my-8 px-20 py-6 font-helios-antique border-t-8 border-sky-900">
        <div className="w-full">
          <h1 className="text-2xl text-white w-fit border-b-2 border-white pb-2">
            {t("h1_1")}
          </h1>
        </div>
        <div className="container w-full flex items-center align-middle justify-between mt-8">
          <div className="flex flex-col items-center px-6 py-4 text-center text-white">
            <span className="text-2xl lg:text-4xl font-bold">00.00%</span>
            <p className="text-xs">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Consequatur soluta aut earum vero eos sint.
            </p>
          </div>
          <div className="border-l-2 border-white h-32"></div>
          <div className="flex flex-col items-center px-6 py-4 text-center text-white">
            <span className="text-2xl lg:text-4xl font-bold">0.000.000</span>
            <p className="text-xs">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Consequatur soluta aut earum vero eos sint.
            </p>
          </div>
          <div className="border-l-2 border-white h-32"></div>
          <div className="flex flex-col items-center px-6 py-4 text-center text-white">
            <span className="text-2xl lg:text-4xl font-bold">000</span>
            <p className="text-xs">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Consequatur soluta aut earum vero eos sint.
            </p>
          </div>
          <div className="border-l-2 border-white h-32"></div>
          <div className="flex flex-col items-center px-6 py-4 text-center text-white">
            <span className="text-2xl lg:text-4xl font-bold">00.00%</span>
            <p className="text-xs">
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Consequatur soluta aut earum vero eos sint.
            </p>
          </div>
        </div>
      </div>
      <div className="w-full px-12 h-96 mt-20">
        <div className="flex flex-col items-center w-11/12 mx-auto">
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
            className="section-container-how-work-it"
          >
            <h1 className="text-4xl font-semibold pb-4 w-auto font-helios-antique text-blue-900 border-b-2 border-blue-900">
              {t("h1_2")}
            </h1>

            <Image
              width={300}
              height={200}
              src="/img/foto2-2.png"
              className=""
              alt=""
            />
          </div>
        </div>
      </div>
      <div className="w-full px-12 mt-20">
        <div className="flex flex-col items-center w-11/12 mx-auto">
          <h1 className="text-4xl font-semibold pb-4 w-auto font-helios-antique text-blue-900 border-b-2 border-blue-900">
            {t("h1_3")}
          </h1>
          <div className="grid grid-cols-2 lg:grid-cols-4 gap-6 w-full mt-12 mb-12">
            <div className="w-full rounded-lg border-2 border-gray-200">
              <div className="mb-12 p-6 md:mb-0 text-center">
                <div className="grid grid-cols-3 gap-2 items-center mb-6">
                  <hr className="border-gray-200" />
                  <Image
                    width={75}
                    height={75}
                    src="https://i.pravatar.cc/100"
                    alt=""
                    className="rounded-full shadow-lg w-24"
                  />
                  <hr className="border-gray-200" />
                </div>
                <p className="mb-4">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod
                  eos id officiis hic tenetur quae quaerat ad velit ab hic
                  tenetur.
                </p>
                <h5 className="text-lg font-bold mb-4">Lorem ipsum</h5>
                <h5 className="text-lg font-bold mb-4">(CEO)</h5>
              </div>
            </div>
            <div className="w-full rounded-lg border-2 border-gray-200">
              <div className="mb-12 p-6 md:mb-0 text-center">
                <div className="grid grid-cols-3 gap-2 items-center mb-6">
                  <hr className="border-gray-200" />
                  <Image
                    width={75}
                    height={75}
                    src="https://i.pravatar.cc/100"
                    alt=""
                    className="rounded-full shadow-lg w-24"
                  />
                  <hr className="border-gray-200" />
                </div>
                <p className="mb-4">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod
                  eos id officiis hic tenetur quae quaerat ad velit ab hic
                  tenetur.
                </p>
                <h5 className="text-lg font-bold mb-4">Lorem ipsum</h5>
                <h5 className="text-lg font-bold mb-4">(CFO)</h5>
              </div>
            </div>
            <div className="w-full rounded-lg border-2 border-gray-200">
              <div className="mb-12 p-6 md:mb-0 text-center">
                <div className="grid grid-cols-3 gap-2 items-center mb-6">
                  <hr className="border-gray-200" />
                  <Image
                    width={75}
                    height={75}
                    src="https://i.pravatar.cc/100"
                    alt=""
                    className="rounded-full shadow-lg w-24"
                  />
                  <hr className="border-gray-200" />
                </div>
                <p className="mb-4">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod
                  eos id officiis hic tenetur quae quaerat ad velit ab hic
                  tenetur.
                </p>
                <h5 className="text-lg font-bold mb-4">Lorem ipsum</h5>
                <h5 className="text-lg font-bold mb-4">(ADVISOR)</h5>
              </div>
            </div>
            <div className="w-full rounded-lg border-2 border-gray-200">
              <div className="mb-12 p-6 md:mb-0 text-center">
                <div className="grid grid-cols-3 gap-2 items-center mb-6">
                  <hr className="border-gray-200" />
                  <Image
                    width={75}
                    height={75}
                    src="https://i.pravatar.cc/100"
                    alt=""
                    className="rounded-full shadow-lg w-24"
                  />
                  <hr className="border-gray-200" />
                </div>
                <p className="mb-4">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod
                  eos id officiis hic tenetur quae quaerat ad velit ab hic
                  tenetur.
                </p>
                <h5 className="text-lg font-bold mb-4">Lorem ipsum</h5>
                <h5 className="text-lg font-bold mb-4">(Creativo)</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export async function getStaticProps({ locale }: GetStaticPropsContext) {
  return {
    props: {
      messages: (await import(`../messages/${locale}.json`)).default,
    },
  };
}
