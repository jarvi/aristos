import { Inter } from '@next/font/google'
import Image from 'next/image'
import IndexCarousel from '../components/index/IndexCarousel'
import { GetStaticPropsContext } from 'next'
import { useTranslations } from 'next-intl'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
    const t = useTranslations('IndexPage');
    return (
        <>
            <div className="w-full flex justify-center relative h-4/5">
                <IndexCarousel />
            </div>
            <div className='w-full px-12 mt-20'>
                <div className='flex flex-col items-center w-11/12 mx-auto'>
                    <h1 className='text-4xl font-semibold pb-4 w-auto font-revalia text-blue-900 border-b-2 border-blue-900'>
                        Aristos
                    </h1>
                    <div className='grid grid-cols-2 px-12 gap-4 mt-20'>
                        <div className='relative h-fit'>
                            <div className='absolute z-0 w-full h-full m-auto opacity-10 flex justify-center items-center'>
                                <Image width={250} height={250} src="/img/logo-no-text-full-color.png" className='w-full h-auto' alt="" />
                            </div>
                            <div className="grid grid-cols-2 gap-2">
                                <Image width={250} height={250} src="/img/foto2-1.png" className='z-10 
                                w-56 h-auto object-cover' alt="" />
                                <Image width={400} height={560} src="/img/foto2-2.png" className='z-10 
                                w-56 h-auto object-cover mt-28 xl:mt-40' alt="" />
                            </div>
                        </div>
                        <div className='col-span-1 font-helios-antique'>
                            <h1 className='text-gray-400 text-base'>
                                <span className='text-2xl'>{t('h1.span')}</span>
                                <br />
                                {t('h1.h1')}
                            </h1>
                            <p className='text-blue-900 text-xs mt-4'>
                                {t('p')}
                            </p>

                            <h2 className='text-gray-400 text-2xl mt-6'>
                            {t('h2')}
                            </h2>
                            <p className='text-blue-900 text-xs mt-4'>
                                {t('p1')}
                            </p>
                            <p className='text-blue-900 text-xs mt-2'>
                                {t('p2')}
                            </p>
                            <p className='text-blue-900 text-xs mt-2'>
                                {t('p3')}
                            </p>
                            <p className='text-blue-900 text-xs mt-2'>
                               {t('p4')}
                            </p>

                            <h2 className='text-gray-400 text-2xl mt-6'>
                                {t('p5')}
                            </h2>
                            <p className='text-blue-900 text-xs mt-4'>
                                {t('p6')}
                            </p>
                            <p className='text-blue-900 text-xs mt-2'>
                                {t('p7')}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-full bg-[url('/img/foto1.jpg')] bg-cover my-8 px-20 py-6 font-helios-antique border-t-8 border-sky-900">
                <div className='w-full'>
                    <h1 className='text-2xl text-white w-fit border-b-2 border-white pb-2'>{t('h1_1')}</h1>
                </div>
                <div className="container w-full flex items-center align-middle justify-between mt-8">
                    <div className='flex flex-col items-center px-6 py-4 text-center text-white'>
                        <span className='text-2xl lg:text-4xl font-bold'>00.00%</span>
                        <p className='text-xs'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur soluta aut earum vero eos sint.</p>
                    </div>
                    <div className='border-l-2 border-white h-32'></div>
                    <div className='flex flex-col items-center px-6 py-4 text-center text-white'>
                        <span className='text-2xl lg:text-4xl font-bold'>0.000.000</span>
                        <p className='text-xs'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur soluta aut earum vero eos sint.</p>
                    </div>
                    <div className='border-l-2 border-white h-32'></div>
                    <div className='flex flex-col items-center px-6 py-4 text-center text-white'>
                        <span className='text-2xl lg:text-4xl font-bold'>000</span>
                        <p className='text-xs'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur soluta aut earum vero eos sint.</p>
                    </div>
                    <div className='border-l-2 border-white h-32'></div>
                    <div className='flex flex-col items-center px-6 py-4 text-center text-white'>
                        <span className='text-2xl lg:text-4xl font-bold'>00.00%</span>
                        <p className='text-xs'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Consequatur soluta aut earum vero eos sint.</p>
                    </div>
                </div>
            </div>
            <div className='w-full px-12 h-96 mt-20'>
                <div className='flex flex-col items-center w-11/12 mx-auto'>
                    <h1 className='text-4xl font-semibold pb-4 w-auto font-helios-antique text-blue-900 border-b-2 border-blue-900'>
                        {t('h1_2')}
                    </h1>
                </div>
            </div>
            <div className='w-full px-12 mt-20'>
                <div className='flex flex-col items-center w-11/12 mx-auto'>
                    <h1 className='text-4xl font-semibold pb-4 w-auto font-helios-antique text-blue-900 border-b-2 border-blue-900'>
                        {t('h1_3')}
                    </h1>
                    <div className="grid grid-cols-2 lg:grid-cols-4 gap-6 w-full mt-12 mb-12">
                        <div className="w-full rounded-lg border-2 border-gray-200">
                            <div className="mb-12 p-6 md:mb-0 text-center">
                                <div className="grid grid-cols-3 gap-2 items-center mb-6">
                                    <hr className='border-gray-200' />
                                    <Image width={75} height={75} src="https://i.pravatar.cc/100" alt="" className="rounded-full shadow-lg w-24" />
                                    <hr className='border-gray-200' />
                                </div>
                                <p className="mb-4">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic
                                    tenetur quae quaerat ad velit ab hic tenetur.
                                </p>
                                <h5 className="text-lg font-bold mb-4">Lorem ipsum</h5>
                                <h5 className="text-lg font-bold mb-4">(CEO)</h5>
                            </div>
                        </div>
                        <div className="w-full rounded-lg border-2 border-gray-200">
                            <div className="mb-12 p-6 md:mb-0 text-center">
                                <div className="grid grid-cols-3 gap-2 items-center mb-6">
                                    <hr className='border-gray-200' />
                                    <Image width={75} height={75} src="https://i.pravatar.cc/100" alt="" className="rounded-full shadow-lg w-24" />
                                    <hr className='border-gray-200' />
                                </div>
                                <p className="mb-4">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic
                                    tenetur quae quaerat ad velit ab hic tenetur.
                                </p>
                                <h5 className="text-lg font-bold mb-4">Lorem ipsum</h5>
                                <h5 className="text-lg font-bold mb-4">(CFO)</h5>
                            </div>
                        </div>
                        <div className="w-full rounded-lg border-2 border-gray-200">
                            <div className="mb-12 p-6 md:mb-0 text-center">
                                <div className="grid grid-cols-3 gap-2 items-center mb-6">
                                    <hr className='border-gray-200' />
                                    <Image width={75} height={75} src="https://i.pravatar.cc/100" alt="" className="rounded-full shadow-lg w-24" />
                                    <hr className='border-gray-200' />
                                </div>
                                <p className="mb-4">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic
                                    tenetur quae quaerat ad velit ab hic tenetur.
                                </p>
                                <h5 className="text-lg font-bold mb-4">Lorem ipsum</h5>
                                <h5 className="text-lg font-bold mb-4">(ADVISOR)</h5>
                            </div>
                        </div>
                        <div className="w-full rounded-lg border-2 border-gray-200">
                            <div className="mb-12 p-6 md:mb-0 text-center">
                                <div className="grid grid-cols-3 gap-2 items-center mb-6">
                                    <hr className='border-gray-200' />
                                    <Image width={75} height={75} src="https://i.pravatar.cc/100" alt="" className="rounded-full shadow-lg w-24" />
                                    <hr className='border-gray-200' />
                                </div>
                                <p className="mb-4">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quod eos id officiis hic
                                    tenetur quae quaerat ad velit ab hic tenetur.
                                </p>
                                <h5 className="text-lg font-bold mb-4">Lorem ipsum</h5>
                                <h5 className="text-lg font-bold mb-4">(Creativo)</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}


export async function getStaticProps({ locale }: GetStaticPropsContext) {
    return {
        props: {
            messages: (await import(`../messages/${locale}.json`)).default
        }
    };
}
