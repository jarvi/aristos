import { faAngleDown, faAngleUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Inter } from '@next/font/google'
import Image from 'next/image'
import { useState } from 'react'
import { GetStaticPropsContext } from 'next'
import { useTranslations } from 'next-intl'


const inter = Inter({ subsets: ['latin'] })

export default function Emitter() {
    const t = useTranslations('EmitterPage');
    const [isHidden, setIsHidden] = useState(true)

    return (
        <>
            <div className='w-full px-12 mt-20 font-lato leading-none'>
                <div className='w-11/12 mx-auto'>
                    <div className='flex justify-between items-center w-full mx-auto px-6 text-center font-bold text-blue-900'>
                        <h3 className="w-1/6">{t('div.h3')}</h3>
                        <h3 className="w-1/6">{t('div.h3_1')}</h3>
                        <h3 className="w-1/6">{t('div.h3_2')}</h3>
                        <h3 className="w-1/6">{t('div.h3_3')}</h3>
                        <h3 className="w-1/6">{t('div.h3_4')}</h3>
                        <h3 className="w-1/6">{t('div.h3_5')}</h3>
                    </div>
                    <div className="grid grid-cols-6 items-center justify-items-center w-full mx-auto mt-6 p-6 text-center text-blue-900 bg-white shadow-xl rounded-lg">
                        <p>LoanBook</p>
                        <Image width={48} height={48} src="/img/flag_icon_round_usa.png" alt="USA flag" />
                        <p>37</p>
                        <p>Factoraje</p>
                        <p>US$ 8MM</p>
                        <p>13.00%</p>
                    </div>
                </div>
            </div>
            <div className='w-full px-12 my-10 font-lato leading-none'>
                <div className={`w-11/12 mx-auto flex justify-between relative bg-white shadow-xl rounded-lg transition duration-700 ease-linear ${isHidden ? '' : 'h-fit'}`}>
                    <h1 className={`p-8 text-gray-400 text-4xl font-bold ${isHidden ? '' : 'hidden'}`}>{t('div_leading.div_ease_linear_1.hi')}</h1>
                    <div className={`w-3/5 mx-auto p-8 text-justify text-blue-900 transition duration-700 ease-linear ${isHidden ? 'hidden' : ''}`}>
                        <h1 className='mb-6 text-gray-400 text-4xl font-bold'>{t('div_leading.div_ease_linear_2.hi')}</h1>
                        <p className='mb-6'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente, reiciendis incidunt modi a veritatis atque earum quos, accusantium, ratione eius odit. Repellat rem aperiam, hic eius velit at accusamus voluptate!</p>
                        <p className='mb-6'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente, reiciendis incidunt modi a veritatis atque earum quos, accusantium, ratione eius odit. Repellat rem aperiam, hic eius velit at accusamus voluptate!</p>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente, reiciendis incidunt modi a veritatis atque earum quos, accusantium, ratione eius odit. Repellat rem aperiam, hic eius velit at accusamus voluptate!</p>
                    </div>
                    <div className={`w-2/5 mx-auto p-6 text-justify text-blue-900 transition duration-700 ease-linear ${isHidden ? 'hidden' : ''}`}>
                        <h1 className='mb-6 text-gray-400 text-2xl font-bold'>{t('div_leading.div_ease_linear_2.hi_1')}</h1>
                        <ul>
                            <li className='mb-6'>{t('div_leading.div_ease_linear_2.ul.li')}</li>
                            <li className='mb-6'>{t('div_leading.div_ease_linear_2.ul.li_1')}</li>
                            <li className='mb-6'>{t('div_leading.div_ease_linear_2.ul.li_2')}</li>
                            <li className='mb-6'>{t('div_leading.div_ease_linear_2.ul.li_3')}</li>
                            <li>{t('div_leading.div_ease_linear_2.ul.li_4')}</li>
                        </ul>
                    </div>
                    <button className='border-none absolute right-6 top-6' onClick={() => setIsHidden(!isHidden)}>
                        <FontAwesomeIcon className='w-5 h-5' icon={isHidden ? faAngleDown : faAngleUp} />
                    </button>
                </div>
            </div>
        </>
    )
}


export async function getStaticProps({ locale }: GetStaticPropsContext) {
    return {
        props: {
            messages: (await import(`../messages/${locale}.json`)).default
        }
    };
}
