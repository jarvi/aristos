import type { NextApiRequest, NextApiResponse } from 'next'
import loginController from '../../controllers/login'

type Data = {
  name: string
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  return loginController(req, res)
}