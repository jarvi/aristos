import type { NextApiRequest, NextApiResponse } from 'next'
import registerController from '../../controllers/register'

type Data = {
  name: string
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  return registerController(req, res)
}