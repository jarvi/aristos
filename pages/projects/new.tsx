import React, { use, useEffect, useState } from "react";
import Image from "next/image";
import { Fragment } from "react";
import { GetStaticPropsContext } from "next";
import Link from "next/link";
import { useTranslations } from "next-intl";

import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import { useForm } from "react-hook-form";

import { faFlag } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import useHttpPost from "../../hooks/useHttpPost";
import { APIs, EndPoints } from "../../util/constants";

import LoadingSpinner from "../../components/spinner/Spinner";
import emulateHttpRequest from "../../helpers/emulateHttpRequest";

type FormInputs = {
  companyName: string;
};

function NewProject() {
  const [loading, setLoading] = useState(false);
  const t = useTranslations("NewProjects");
  const DESCRIPTION_MAX_LENGTH = 1000;
  const [descriptionCharacterCount, setDescriptionCharacterCount] = useState(0);
  const [dateFormPrepared, setDateFormPrepared] = useState<any>(null);

  const { response, error } = useHttpPost(
    `${APIs.mockapiPosts}${EndPoints.post}`,
    dateFormPrepared
  );

  const [base64, setBase64] = useState("");

  const schema = Yup.object({
    companyName: Yup.string()
      .required(t("msgR"))
      .min(10, t("msgMin"))
      .max(50, t("msgMax")),
    description: Yup.string()
      .required(t("msgR"))
      .min(10, t("msg4"))
      .max(1000, t("msg4")),
    createdBy: Yup.string().required(t("msg6")),
    amount: Yup.number().required(t("msg7")),
    url: Yup.string().required(t("msgR")).url(t("msg9")),
    phoneNumber: Yup.string()
      .required(t("msgR"))
      .matches(/^[0-9]+$/, t("msg11"))
      .max(8, t("msg12"))
      .min(8, t("msg13")),
    email: Yup.string().required(t("msgR")).email(t("msg8")),
    code: Yup.string(),
    country: Yup.string(),
    privaciConditions: Yup.string().matches(/true/, t("msg16")),
  }).required();

  const {
    setError,
    setValue,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = async (data: any) => {
    console.log("handleForm");
    console.log(data);

    let dataForm = {
      ...data,
    };

    console.log(data);
    setLoading(true);
    const serverRequest = await emulateHttpRequest("");
    setDateFormPrepared(dataForm);
    setLoading(false);
    console.log(response);
  };

  useEffect(() => {
    console.log(errors);
  }, [errors]);

  async function getBase64FomFile(img: any): Promise<string> {
    //console.log("entro en getBase64FomFile");
    const fileReader = new FileReader();
    const result = await new Promise<string>((resolve, reject) => {
      fileReader.onload = () => {
        resolve(fileReader.result as string);
      };
      fileReader.onerror = reject;
      fileReader.readAsDataURL(img);
    });

    return result;
  }

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="flex w-full justify-around px-12 mt-14 mb-14 font-helios-antique">
          <div className="text-blue-900 italic">
            <p className="text-5xl">{t("p")}</p>
            <p className="text-3xl">{t("p1")}</p>
          </div>
          <p className="text-blue-900 text-xl w-2/5 leading-tight">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque
            voluptatem corrupti voluptatibus, ea vitae ullam temporibus ipsum
            necessitatibus iste. Facilis, temporibus
          </p>
        </div>
        <div className="w-full px-20 mt-14 mb-14 font-helios-antique text-blue-900 italic">
          <div className="max-w-5xl grid grid-cols-3 w-full mx-auto mb-12 justify-around items-center">
            <label htmlFor="name" className="w-full flex flex-col">
              <span className="text-5xl"> {t("span")} </span>
              <span className="text-3xl">{t("span1")}</span>
            </label>
            <input
              placeholder="Nombre de la empresa"
              type="text"
              id="name"
              {...register("companyName")}
              className={`col-span-2 w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400 ${
                errors.companyName
                  ? "border border-red-700 focus:outline-none focus:shadow-outline"
                  : ""
              }`}
            />
            <div className="text-red-700 text-sm">
              {errors.companyName?.message?.toString()}
            </div>
          </div>
          <div className="max-w-5xl grid grid-cols-3 w-full mx-auto mb-12 justify-around items-center">
            <label htmlFor="description" className="w-full flex flex-col">
              <span className="text-3xl">{t("span2")}</span>
              <span className="text-5xl">{t("span3")}</span>
              <p className="w-3/4 mt-4 text-sm">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo
                earum optio incidunt aspernatur magnam ut vero quaerat quas
                architecto dolor quos, asperiores fuga, ipsam eaque suscipit
                doloribus voluptatum deleniti enim.
              </p>
            </label>
            <div className="col-span-2 flex flex-col w-full h-full">
              <textarea
                {...register("description", {
                  onChange: ({ target: { value } }) =>
                    setDescriptionCharacterCount(value.length),
                })}
                id="description"
                placeholder="Description"
                className="w-full h-5/6 p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400"
              />

              <span className="text-xs self-end">
                {descriptionCharacterCount}/{DESCRIPTION_MAX_LENGTH}
              </span>

              <div className="text-red-700 text-sm">
                {errors.description?.message?.toString()}
              </div>
            </div>
          </div>
          <div className="max-w-5xl grid grid-cols-3 w-full mx-auto mb-12 justify-around items-center">
            <label htmlFor="createdBy" className="w-full flex flex-col">
              <span className="text-5xl">{t("span4")}</span>
              <span className="text-xl">{t("span5")}</span>
            </label>
            <div className="col-span-2 w-full grid grid-cols-2 gap-4">
              <div className="py-2 pl-6 flex items-center border bg-zinc-50 border-gray-400 rounded-md text-lg">
                <input
                  {...register("createdBy")}
                  value={"naturalPerson"}
                  type="radio"
                  id="createdByRadio1"
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
                />
                <label htmlFor="createdByRadio1" className="ml-4">
                  {t("label")}
                </label>
              </div>
              <div className="py-2 pl-6 flex items-center border bg-zinc-50 border-gray-400 rounded-md text-lg">
                <input
                  {...register("createdBy")}
                  value={"company"}
                  type="radio"
                  id="createdByRadio2"
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
                />
                <label htmlFor="createdByRadio2" className="ml-4">
                  {t("label1")}
                </label>
              </div>
              <div className="text-red-700 text-sm">
                {errors.createdBy?.message?.toString()}
              </div>
            </div>
          </div>
          <div className="max-w-5xl grid grid-cols-3 w-full mx-auto mb-12 justify-around items-start">
            <label htmlFor="" className="w-full flex flex-col">
              <span className="text-5xl"> {t("span6")} </span>
              <span className="text-base">{t("span7")} </span>
            </label>
            <div className="col-span-2 w-full grid grid-cols-2 gap-4">
              <div className="py-2 pl-6 flex items-center border bg-zinc-50 border-gray-400 rounded-md text-lg">
                <input
                  {...register("amount")}
                  type="radio"
                  id="amountRadio1"
                  value={10000}
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
                />
                <label htmlFor="amountRadio1" className="ml-4">
                  $000000000
                </label>
              </div>
              <div className="py-2 pl-6 flex items-center border bg-zinc-50 border-gray-400 rounded-md text-lg">
                <input
                  {...register("amount")}
                  type="radio"
                  id="amountRadio2"
                  value={20000}
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
                />
                <label htmlFor="amountRadio2" className="ml-4">
                  $200000000
                </label>
              </div>
              <div className="py-2 pl-6 flex items-center border bg-zinc-50 border-gray-400 rounded-md text-lg">
                <input
                  {...register("amount")}
                  type="radio"
                  id="amountRadio3"
                  value={30000}
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
                />
                <label htmlFor="amountRadio3" className="ml-4">
                  $300000000
                </label>
              </div>
              <div className="py-2 pl-6 flex items-center border bg-zinc-50 border-gray-400 rounded-md text-lg">
                <input
                  {...register("amount")}
                  type="radio"
                  id="amountRadio4"
                  value={40000}
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
                />
                <label htmlFor="amountRadio4" className="ml-4">
                  $400000000
                </label>
              </div>
              <div className="py-2 pl-6 flex items-center border bg-zinc-50 border-gray-400 rounded-md text-lg">
                <input
                  {...register("amount")}
                  type="radio"
                  id="amountRadio5"
                  value={50000}
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
                />
                <label htmlFor="amountRadio5" className="ml-4">
                  $500000000
                </label>
              </div>
              <div className="py-2 pl-6 flex items-center border bg-zinc-50 border-gray-400 rounded-md text-lg">
                <input
                  {...register("amount")}
                  type="radio"
                  id="amountRadio6"
                  value={60000}
                  className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2"
                />
                <label htmlFor="amountRadio6" className="ml-4">
                  $600000000
                </label>
              </div>
              <div className="text-red-700 text-sm">
                {errors.amount?.message?.toString()}
              </div>
            </div>
          </div>
          <div className="max-w-5xl grid grid-cols-3 w-full mx-auto mb-12 justify-around items-center">
            <label htmlFor="url" className="w-full flex flex-col">
              <span className="text-5xl">{t("span8")}</span>
            </label>
            <input
              {...register("url")}
              type="text"
              id="url"
              placeholder="https://"
              className={`col-span-2 w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400 ${
                errors.url
                  ? "border border-red-700 focus:outline-none focus:shadow-outline"
                  : ""
              }`}
            />

            <div className="text-red-700 text-sm">
              {errors.url?.message?.toString()}
            </div>
          </div>
          <div className="max-w-5xl grid grid-cols-3 w-full mx-auto mb-12 justify-around items-center">
            <label htmlFor="region" className="w-full flex flex-col">
              <span className="text-4xl"> {t("span9")} </span>
              <span className="text-2xl"> {t("span10")} </span>
            </label>
            <div className="col-span-2 w-full grid grid-cols-2 gap-4">
              <select
                className="w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400"
                {...register("sector")}
              >
                <option className="w-5 h-5 mr-2" value="Construcción">
                  Construcción
                </option>
                <option className="w-5 h-5 mr-2" value="Educación ">
                  Educación
                </option>
              </select>
              {/* <input
              type="text"
              id="region"
              name="region"
              placeholder="SECTOR"
              className="w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400"
            /> */}
              <div className="flex items-center">
                <label htmlFor="country" className="mr-4 text-2xl">
                  {t("label2")}
                </label>

                <select
                  className="w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400"
                  {...register("country")}
                >
                  <option className="w-5 h-5 mr-2" value="Spain">
                    Spain
                  </option>
                  <option className="w-5 h-5 mr-2" value="USA">
                    USA
                  </option>
                </select>
              </div>
            </div>
          </div>
          <div className="max-w-5xl grid grid-cols-3 gap-6 w-full mx-auto mb-12">
            <div className="grid grid-cols-2 col-span-2 w-full mx-auto mb-8">
              <label htmlFor="region" className="w-full flex flex-col">
                <span className="text-4xl">{t("label3")}</span>
              </label>
              <div className="w-full">
                <input
                  {...register("email")}
                  type="text"
                  id="email"
                  placeholder="Email"
                  //className="w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400"

                  className={`col-span-2 w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400 ${
                    errors.email
                      ? "border border-red-700 focus:outline-none focus:shadow-outline"
                      : ""
                  }`}
                />
                <div className="text-red-700 text-sm">
                  {errors.email?.message?.toString()}
                </div>
              </div>
            </div>

            <div className="row-span-2 h-full">
              <p className="w-full text-white bg-blue-900 text-center mb-2">
                {t("p2")}
              </p>
              <div className="flex justify-center items-center h-full text-center border border-gray-400 border-dashed">
                <input
                  {...register("image", {
                    onChange: async (e) => {
                      const file = e.target.files[0];
                      const r = await getBase64FomFile(file);
                      setValue("image", r);
                      setBase64(r);
                    },
                  })}
                  type="file"
                  id="image"
                  accept="image/*"
                />
                {base64 && (
                  <Image
                    src={base64}
                    alt="Landscape picture"
                    width={90}
                    height={90}
                  />
                )}
                {t("div.text")} <br /> {t("div.text2")} <br /> {t("div.text1")}
              </div>
            </div>

            <div className="grid grid-cols-2 col-span-2 w-full mx-auto mb-12">
              <label htmlFor="region" className="w-full flex flex-col">
                <span className="text-2xl">{t("span11")}</span>
              </label>
              <div className="w-full flex">
                {/* <span className="flex p-2 w-2/6 border border-r-0 border-gray-400 rounded-l-md">
                <FontAwesomeIcon className="w-5 h-5 mr-2" icon={faFlag} />
                SV+503
              </span> */}

                <select
                  className="flex p-2 w-2/2 border border-r-0 border-gray-400 rounded-l-md"
                  {...register("code")}
                >
                  {/* <FontAwesomeIcon className="w-5 h-5 mr-2" icon={faFlag} /> */}
                  <option className="w-5 h-5 mr-2" value="SV+503">
                    SV+503
                  </option>
                  <option className="w-5 h-5 mr-2" value="SV+01">
                    SV+01
                  </option>
                </select>
                <input
                  {...register("phoneNumber")}
                  type="text"
                  id="phoneNumber"
                  placeholder="Phone Number"
                  className={`col-span-2 w-full h-fit p-2 border bg-zinc-50 border-gray-400 rounded-md focus:bg-white focus:border-gray-400 ${
                    errors.phoneNumber
                      ? "border border-red-700 focus:outline-none focus:shadow-outline"
                      : ""
                  }`}
                />
              </div>
              <div className="text-red-700 text-sm">
                {errors.phoneNumber?.message?.toString()}
              </div>
            </div>
          </div>
        </div>
        <div className="w-full px-20 mt-14 mb-14 font-helios-antique text-blue-900">
          <div className="max-w-5xl w-full mx-auto mb-12 justify-around items-center">
            <p className="mb-4 text-base">{t("p3")}</p>
            <p className="text-base">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste
              alias harum facere cum sint quia dignissimos! Facilis at atque
              quae laboriosam, facere, ipsa fugit veniam obcaecati veritatis eos
              in id! Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Iste alias harum facere cum sint quia dignissimos! Facilis at
              atque quae laboriosam, facere, ipsa fugit veniam obcaecati
              veritatis eos in id! Lorem ipsum dolor sit amet consectetur
              adipisicing elit. Iste alias harum facere cum sint quia
              dignissimos! Facilis at atque quae laboriosam, facere, ipsa fugit
              veniam obcaecati veritatis eos in id!
            </p>
          </div>
        </div>
        <div className="w-full px-20 mt-14 mb-14 font-helios-antique text-blue-900">
          <div className="w-fit mx-auto mb-12">
            <input
              {...register("privaciConditions", {
                onChange: ({ target: { checked } }) => {
                  setValue("privaciConditions", !!checked);
                },
              })}
              type="checkbox"
            />
            <label htmlFor="privaciConditions" className=" ml-2 text-base">
              {t("p3")}
            </label>

            <div className="text-red-700 text-sm">
              {errors.privaciConditions?.message?.toString()}
            </div>
          </div>
        </div>
        <div className="w-full px-20 mt-14 mb-14 font-helios-antique text-blue-900">
          <div className="w-fit mx-auto mb-12">
            {/* <input
              className="px-24 py-3 mt-4 font-light text-2xl border-blue-900 bg-blue-900 text-white"
              type="submit"
              value={t("link")}
            /> */}
            <button
              className="px-24 py-3 mt-4 font-light text-2xl border-blue-900 bg-blue-900 text-white"
              type="submit"
              //value={t("link")}
            >
              {loading ? <LoadingSpinner /> : t("link")}
            </button>
          </div>
        </div>
      </form>
    </>
  );
}

export default NewProject;

export async function getStaticProps({ locale }: GetStaticPropsContext) {
  return {
    props: {
      messages: (await import(`../../messages/${locale}.json`)).default,
    },
  };
}
