import { faHeart } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Inter } from '@next/font/google'
import Image from 'next/image'
import Link from 'next/link'
import ProjectsCarousel from '../../components/projects/ProjectsCarousel'
import { GetStaticPropsContext } from 'next'
import { useTranslations } from 'next-intl'
const inter = Inter({ subsets: ['latin'] })

export default function Projects() {
    const t = useTranslations('Projects');
    return (
        <>
            <div className="w-full flex justify-center relative h-4/5">
                <ProjectsCarousel />
            </div>
            <div className='flex flex-col w-full px-12 mt-14 mb-14 font-helios-antique'>
                <div className="flex flex-col justify-start items-start w-full text-center text-blue-900">
                    <p className='text-4xl'>{t('p')}</p>
                    <hr className='w-36 mt-4 border-t-2 border-blue-900' />
                    <div className='grid grid-cols-2 gap-4 mt-10'>
                        <div className='border border-slate-300 rounded-b-lg rounded-t-sm'>
                            <Image width={200} height={200} className='w-full h-96 object-cover rounded-t-sm' src="/img/foto3.jpg" alt="" />
                            <div className='flex justify-between relative p-4 pt-6 bg-white rounded-b-lg'>
                                <div className='text-start w-1/2'>
                                    <h4 className='text-base text-gray-400'>Lorem</h4>
                                    <h3 className='text-xl text-black'>Lorem ipsum</h3>
                                </div>
                                <p className='w-1/2 text-black text-base text-left'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. sed</p>
                                <div className='text-slate-400 bg-white border border-slate-400 rounded-full p-2 absolute right-5 -top-4'>
                                    <FontAwesomeIcon className='w-4 h-4' icon={faHeart} />
                                </div>
                            </div>
                        </div>
                        <div className=''>
                            <div className='mb-4 border border-slate-300 rounded-b-lg rounded-t-sm'>
                                <Image width={200} height={200} className='w-full h-36 object-cover rounded-t-sm' src="/img/foto3.jpg" alt="" />
                                <div className='flex justify-between relative p-4 pt-6 bg-white rounded-b-lg'>
                                    <div className='text-start w-1/2'>
                                        <h4 className='text-sm text-gray-400'>Lorem</h4>
                                        <h3 className='text-lg text-black'>Lorem ipsum</h3>
                                    </div>
                                    <p className='w-1/2 text-black text-sm text-left'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. sed</p>
                                    <div className='text-slate-400 bg-white border border-slate-400 rounded-full p-2 absolute right-5 -top-4'>
                                        <FontAwesomeIcon className='w-4 h-4' icon={faHeart} />
                                    </div>
                                </div>
                            </div>
                            <div className='border border-slate-300 rounded-b-lg rounded-t-sm'>
                                <Image width={200} height={200} className='w-full h-36 object-cover rounded-t-sm' src="/img/foto3.jpg" alt="" />
                                <div className='flex justify-between relative p-4 pt-6 bg-white rounded-b-lg'>
                                    <div className='text-start w-1/2'>
                                        <h4 className='text-sm text-gray-400'>Lorem</h4>
                                        <h3 className='text-lg text-black'>Lorem ipsum</h3>
                                    </div>
                                    <p className='w-1/2 text-black text-sm text-left'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. sed</p>
                                    <div className='text-slate-400 bg-white border border-slate-400 rounded-full p-2 absolute right-5 -top-4'>
                                        <FontAwesomeIcon className='w-4 h-4' icon={faHeart} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='flex flex-col w-full px-12 mt-14 mb-14 font-helios-antique bg-gray-200'>
                <div className="justify-start w-full py-12 text-center text-blue-900">
                    <div className='flex items-baseline justify-start w-full'>
                        <p className='text-2xl'>{t('p')}</p>
                        <p className='text-lg ml-6'>{t('p1')} XXXXXXXXXX</p>
                        <hr className='flex-1 ml-6 border-blue-900' />
                    </div>
                    <div className='grid grid-cols-3 gap-6 mt-4'>
                        <div className='border border-slate-300 rounded-lg'>
                            <Image width={200} height={200} className='w-full h-36 object-cover rounded-t-lg' src="/img/foto3.jpg" alt="" />
                            <div className='flex flex-col justify-between relative p-4 pt-6 bg-white rounded-b-lg'>
                                <div className='text-start'>
                                    <h4 className='text-sm text-gray-400'>Lorem</h4>
                                    <h3 className='text-lg text-black'>Lorem ipsum</h3>
                                </div>
                                <p className='text-black text-sm text-left'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. sed</p>
                                <div className='text-slate-400 bg-white border border-slate-400 rounded-full p-2 absolute right-5 -top-4'>
                                    <FontAwesomeIcon className='w-4 h-4' icon={faHeart} />
                                </div>
                            </div>
                        </div>
                        <div className='border border-slate-300 rounded-lg'>
                            <Image width={200} height={200} className='w-full h-36 object-cover rounded-t-lg' src="/img/foto3.jpg" alt="" />
                            <div className='flex flex-col justify-between relative p-4 pt-6 bg-white rounded-b-lg'>
                                <div className='text-start'>
                                    <h4 className='text-sm text-gray-400'>Lorem</h4>
                                    <h3 className='text-lg text-black'>Lorem ipsum</h3>
                                </div>
                                <p className='text-black text-sm text-left'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. sed</p>
                                <div className='text-slate-400 bg-white border border-slate-400 rounded-full p-2 absolute right-5 -top-4'>
                                    <FontAwesomeIcon className='w-4 h-4' icon={faHeart} />
                                </div>
                            </div>
                        </div>
                        <div className='border border-slate-300 rounded-lg'>
                            <Image width={200} height={200} className='w-full h-36 object-cover rounded-t-lg' src="/img/foto3.jpg" alt="" />
                            <div className='flex flex-col justify-between relative p-4 pt-6 bg-white rounded-b-lg'>
                                <div className='text-start'>
                                    <h4 className='text-sm text-gray-400'>Lorem</h4>
                                    <h3 className='text-lg text-black'>Lorem ipsum</h3>
                                </div>
                                <p className='text-black text-sm text-left'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. sed</p>
                                <div className='text-slate-400 bg-white border border-slate-400 rounded-full p-2 absolute right-5 -top-4'>
                                    <FontAwesomeIcon className='w-4 h-4' icon={faHeart} />
                                </div>
                            </div>
                        </div>
                        <div className='border border-slate-300 rounded-lg'>
                            <Image width={200} height={200} className='w-full h-36 object-cover rounded-t-lg' src="/img/foto3.jpg" alt="" />
                            <div className='flex flex-col justify-between relative p-4 pt-6 bg-white rounded-b-lg'>
                                <div className='text-start'>
                                    <h4 className='text-sm text-gray-400'>Lorem</h4>
                                    <h3 className='text-lg text-black'>Lorem ipsum</h3>
                                </div>
                                <p className='text-black text-sm text-left'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. sed</p>
                                <div className='text-slate-400 bg-white border border-slate-400 rounded-full p-2 absolute right-5 -top-4'>
                                    <FontAwesomeIcon className='w-4 h-4' icon={faHeart} />
                                </div>
                            </div>
                        </div>
                        <div className='border border-slate-300 rounded-lg'>
                            <Image width={200} height={200} className='w-full h-36 object-cover rounded-t-lg' src="/img/foto3.jpg" alt="" />
                            <div className='flex flex-col justify-between relative p-4 pt-6 bg-white rounded-b-lg'>
                                <div className='text-start'>
                                    <h4 className='text-sm text-gray-400'>Lorem</h4>
                                    <h3 className='text-lg text-black'>Lorem ipsum</h3>
                                </div>
                                <p className='text-black text-sm text-left'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. sed</p>
                                <div className='text-slate-400 bg-white border border-slate-400 rounded-full p-2 absolute right-5 -top-4'>
                                    <FontAwesomeIcon className='w-4 h-4' icon={faHeart} />
                                </div>
                            </div>
                        </div>
                        <div className='border border-slate-300 rounded-lg'>
                            <Image width={200} height={200} className='w-full h-36 object-cover rounded-t-lg' src="/img/foto3.jpg" alt="" />
                            <div className='flex flex-col justify-between relative p-4 pt-6 bg-white rounded-b-lg'>
                                <div className='text-start'>
                                    <h4 className='text-sm text-gray-400'>Lorem</h4>
                                    <h3 className='text-lg text-black'>Lorem ipsum</h3>
                                </div>
                                <p className='text-black text-sm text-left'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. sed</p>
                                <div className='text-slate-400 bg-white border border-slate-400 rounded-full p-2 absolute right-5 -top-4'>
                                    <FontAwesomeIcon className='w-4 h-4' icon={faHeart} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='w-full mt-4'>
                        <button
                            className="px-8 py-2 mt-4 rounded-lg font-light text-lg border border-blue-900 bg-gray-200 text-blue-900"
                            type="button"
                        >
                            {t('button')}
                        </button>
                    </div>
                </div>
            </div>
            <div className='grid grid-cols-2 w-full px-12 mt-24 mb-14 font-helios-antique'>
                <Image width={200} height={200} className='mx-auto' src="/img/foto2-1.png" alt="" />
                <div className='flex flex-col justify-center items-start px-8 text-blue-900'>
                    <p className='text-4xl w-fit mb-8'>{t('p2')}</p>
                    <p className='text-base mb-2'>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quod quae quidem eos repellat, molestias repudiandae illum, magni, mollitia incidunt quibusdam vero quaerat recusandae dolore deleniti sint totam? Placeat, porro ratione!</p>
                    <Link
                        className="px-8 py-1 mt-4 rounded-md font-light text-base border-blue-900 bg-blue-900 text-white"
                        type="button"
                        href='/projects/new'
                    >
                        {t('link')}
                    </Link>
                </div>
            </div>
        </>
    )
}


export async function getStaticProps({ locale }: GetStaticPropsContext) {
    return {
        props: {
            messages: (await import(`../../messages/${locale}.json`)).default
        }
    };
}
