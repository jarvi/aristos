import { faCircle } from "@fortawesome/free-solid-svg-icons";
import { faMoneyBill } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import CompanyFundingForm from "../components/funding/CompanyFundingForm";
import UserFundingForm from "../components/funding/UserFundingForm";
import { GetStaticPropsContext } from "next";
import { useTranslations } from "next-intl";
import { FileTemplateNames } from "../util/constants";
import UploadDocument from "../components/uploadDocument/UploadDocument";
import CardTemplate from "../components/cardTemplate/CardTemplate";

export default function Funding() {
  const t = useTranslations("fundingPage");

  const DocumentsRepresentative: any = [
    t("ul_1.li"),
    t("ul_1.li_1"),
    t("ul_1.li_2"),
    t("ul_1.li_3"),
  ];

  const DocumentsGenerals: any = [
    t("ul.li"),
    t("ul.li_1"),
    t("ul.li_2"),
    t("ul.li_3"),
    t("ul.li_4"),
    t("ul.li_5"),
    t("ul.li_6"),
    t("ul.li_7"),
    t("ul.li_8"),
  ];

  const title = {
    p: t("p32"),
    p1: t("p33"),
  };

  const title2 = {
    p: t("p30"),
    p1: t("p31"),
  };

  const validLengthRepresentativeDocuments: number = 4;
  const validLengthGeneralDocuments: number = 9;

  const templatesEvaluation = {
    word: FileTemplateNames["TemplateOne"],
    excel: FileTemplateNames["TemplateTwo"],
    p: t("p19"),
    p1: t("p20"),
  };

  const requestFinancing = {
    word: FileTemplateNames["TemplateThree"],
    excel: FileTemplateNames["TemplateFour"],
    p: t("p21"),
    p1: t("p22"),
  };

  const KYC = {
    word: FileTemplateNames["TemplateFive"],
    excel: FileTemplateNames["TemplateSix"],
    p: "KYC",
  };

  const shareInformation = {
    word: FileTemplateNames["TemplateSeven"],
    excel: FileTemplateNames["TemplateEight"],
    p: t("p24"),
    p1: t("p25"),
  };

  const statementSworn = {
    word: FileTemplateNames["TemplateNine"],
    excel: FileTemplateNames["TemplateTen"],
    p: t("p26"),
    p1: t("p27"),
  };

  return (
    <>
      <div className="w-full px-12 mt-24 mb-14 font-helios-antique">
        <div className="leading-none text-blue-900 border-2 border-blue-900 rounded-3xl relative">
          <div className="absolute -top-20 w-full">
            <div className="w-72 mx-auto text-center bg-zinc-50 p-12">
              <p className="text-4xl">{t("p")}</p>
            </div>
          </div>
          <div className="grid grid-cols-2 grid-rows-2 gap-16 space-y-4 mt-14 w-full px-16 pb-12 text-center">
            <div className="flex flex-col justify-center items-center">
              <div className="border-2 border-blue-900 rounded-full p-1">
                <FontAwesomeIcon className="w-24 h-24" icon={faMoneyBill} />
              </div>
              <p className="text-3xl">{t("p1")}</p>
              <p>{t("p2")}</p>
            </div>
            <div className="flex flex-col justify-center items-center">
              <div className="border-2 border-blue-900 rounded-full p-1">
                <FontAwesomeIcon className="w-24 h-24" icon={faMoneyBill} />
              </div>
              <p className="text-3xl">{t("p3")}</p>
              <p>{t("p4")}</p>
            </div>
            <div className="flex flex-col justify-center items-center">
              <div className="border-2 border-blue-900 rounded-full p-1">
                <FontAwesomeIcon className="w-24 h-24" icon={faMoneyBill} />
              </div>
              <p className="text-3xl">{t("p5")}</p>
              <p>{t("p7")}</p>
            </div>
            <div className="flex flex-col justify-center items-center">
              <div className="border-2 border-blue-900 rounded-full p-1">
                <FontAwesomeIcon className="w-24 h-24" icon={faMoneyBill} />
              </div>
              <p className="text-3xl">{t("p8")}</p>
              <p>{t("p9")}</p>
            </div>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-2 w-full px-12 mt-24 mb-14 font-helios-antique">
        <div className="flex flex-col justify-center items-center relative">
          <Image
            fill
            className="mx-auto object-contain"
            src="/img/foto2-1.png"
            alt=""
          />
        </div>
        <div className="flex flex-col justify-center items-center text-center text-blue-900">
          <p className="text-xl w-fit px-8 py-2 mb-8 rounded-full bg-gray-200">
            {t("p10")}
          </p>
          <div className="flex flex-col justify-center items-center text-center relative">
            <FontAwesomeIcon
              className="w-24 h-24 absolute bottom-0 left-0 opacity-20 text-gray-400"
              icon={faMoneyBill}
            />
            <p className="text-4xl">{t("p11")}</p>
            <p className="text-lg w-72">{t("p12")}</p>
          </div>
        </div>
      </div>
      <div className="flex flex-col justify-start items-center w-full px-12 mt-24 mb-14 font-helios-antique ">
        <div className="flex flex-col justify-start items-center w-full py-12 text-center text-blue-900 bg-gray-200">
          <p className="text-4xl">{t("p13")}</p>
          <hr className="w-28 mt-4 border-t-2 border-blue-900" />
          <div className="grid grid-cols-2 gap-12 px-12 mt-8 lg:mt-12">
            <div className="flex flex-col w-fit p-6 justify-center items-center shadow-md bg-white">
              <p className="text-3xl w-fit px-8 py-2 text-gray-400">
                {t("p14")}
              </p>
              <div className="flex flex-col justify-center items-center text-center">
                <ul className="text-sm flex flex-col items-center">
                  <li className="w-fit">
                    <FontAwesomeIcon
                      className="w-1 h-1 mr-2 inline"
                      icon={faCircle}
                    />
                    <span>{t("span")}</span>
                  </li>
                  <li className="w-fit">
                    <FontAwesomeIcon
                      className="w-1 h-1 mr-2 inline"
                      icon={faCircle}
                    />
                    <span>{t("span1")}</span>
                  </li>
                  <li className="w-fit">
                    <FontAwesomeIcon
                      className="w-1 h-1 mr-2 inline"
                      icon={faCircle}
                    />
                    <span>{t("span3")}</span>
                  </li>
                </ul>
                <UserFundingForm />
              </div>
            </div>
            <div className="flex flex-col w-fit p-6 justify-center items-center shadow-md bg-white">
              <p className="text-3xl w-fit px-8 py-2 text-gray-400">
                {t("p15")}
              </p>
              <div className="flex flex-col justify-center items-center text-center">
                <ul className="text-sm flex flex-col items-center">
                  <li className="w-fit">
                    <FontAwesomeIcon
                      className="w-1 h-1 mr-2 inline"
                      icon={faCircle}
                    />
                    <span>{t("span4")}</span>
                  </li>
                  <li className="w-fit">
                    <FontAwesomeIcon
                      className="w-1 h-1 mr-2 inline"
                      icon={faCircle}
                    />
                    <span>{t("span5")}</span>
                  </li>
                  <li className="w-fit">
                    <FontAwesomeIcon
                      className="w-1 h-1 mr-2 inline"
                      icon={faCircle}
                    />
                    <span>{t("span6")}</span>
                  </li>
                </ul>
                <CompanyFundingForm />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="w-full px-12 mt-24 mb-4 font-helios-antique">
        <div className="flex flex-col justify-start items-center w-full py-12 text-center text-blue-900">
          <p className="text-4xl">{t("p17")}</p>
          <hr className="w-28 mt-4 border-t-2 border-blue-900" />
          <div className="flex flex-col justify-start items-center w-full mt-12 text-center border-2 border-gray-200 rounded-xl relative">
            <div className="absolute -top-6 w-full">
              <div className="w-fit mx-auto text-center bg-zinc-50 p-2">
                <p className="text-3xl text-gray-400">{t("p18")}</p>
              </div>
            </div>
            <div className="grid grid-cols-3 justify-items-center lg:flex flex-wrap w-full justify-between p-7 mt-8 lg:mt-12">
              <CardTemplate arguments={templatesEvaluation} />
              <CardTemplate arguments={requestFinancing} />
              <CardTemplate arguments={KYC} />
              <CardTemplate arguments={shareInformation} />
              <CardTemplate arguments={statementSworn} />
            </div>
          </div>
        </div>
      </div>
      <div className="w-full px-12 mb-4 font-helios-antique">
        <div className="flex justify-evenly items-start w-full py-12 text-center text-blue-900">
          <div className="w-2/5 text-center bg-zinc-50 p-2">
            <p className="text-2xl text-gray-400">
              {t("p28")} descargar <br />
              {t("p28_5")}
            </p>
            <p className="text-4xl text-gray-400">{t("p29")}</p>
          </div>
          <div className="gap-x-5 flex justify-center items-start w-3/5 mb-6">
            <UploadDocument
              documents={DocumentsGenerals}
              lengthValid={validLengthGeneralDocuments}
              title={title2}
            />
            <UploadDocument
              documents={DocumentsRepresentative}
              lengthValid={validLengthRepresentativeDocuments}
              title={title}
            />
          </div>
        </div>
      </div>
    </>
  );
}

export async function getStaticProps({ locale }: GetStaticPropsContext) {
  return {
    props: {
      messages: (await import(`../messages/${locale}.json`)).default,
    },
  };
}
