export const APIs = {
  mockapiPosts: "https://643c5bc8f0ec48ce9042d8f2.mockapi.io/",
};

export const EndPoints = {
  post: "digital/posts/",
};

export const DocumentsGenerals: any = {
  DUI: "DUI",
  Tax_ID: "Tax_ID",
  RUC: "RUC",
  Constitution_deed: "Constitution_deed",
  Board_credentials: "Board_credentials",
  General_power: "General_power",
  Financial_statements: "Financial_statements",
  Vat_declarations: "Vat_declarations",
  Income_statements: "Income_statements",
};

export const DocumentsRepresentative: any = {
  DUI: "DUI",
  NIT: "NIT",
  Passport: "Passport",
  Resident_Card: "Resident_Card",
};

export enum FileTemplateNames {
  "TemplateOne" = "plantillas_evaluacion.docx",
  "TemplateTwo" = "plantillas_evaluacion.xlsx",
  "TemplateThree" = "solicitud_financiacion.docx",
  "TemplateFour" = "solicitud_financiacion.xlsx",
  "TemplateFive" = "KYC.docx",
  "TemplateSix" = "KYC.xlsx",
  "TemplateSeven" = "CompartirInformacion.docx",
  "TemplateEight" = "CompartirInformacion.xlsx",
  "TemplateNine" = "DeclaracionJurada.docx",
  "TemplateTen" = "DeclaracionJurada.xlsx",
}

export const Lenguajes: any = {
  "/funding": "en",
  "/fr/funding": "fr",
  "/es/funding": "es",
};
