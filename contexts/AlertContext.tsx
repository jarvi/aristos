import React, { useState } from 'react';

const AlertStatus = {
  Success: "SUCCESS",
  Error: "ERROR",
  None: "NONE"
}

type Props = {
  children: JSX.Element
}

const AlertContext = React.createContext<any>(null);
AlertContext.displayName = 'AlertContext';

const AlertProvider = ({ children }: Props) => {
  const [alert, setAlert] = useState(AlertStatus.None);
  const [alertText, setAlertText] = useState<string | null>(null);

  return (
    <AlertContext.Provider
      value={{
        alert: alert,
        alertText: alertText,
        success: (text: string, timeout: number) => {
          setAlertText(text);
          setAlert(AlertStatus.Success);
          setTimeout(() => {
            setAlert(AlertStatus.None);
          }, timeout * 1000 || 10000)

        },
        error: (text: string, timeout: number) => {
          setAlertText(text);
          setAlert(AlertStatus.Error);
          setTimeout(() => {
            setAlert(AlertStatus.None);
          }, timeout * 1000 || 10000)
        },
        clear: () => (setAlert(AlertStatus.None)),
      }}
    >
      {children}
    </AlertContext.Provider>
  );
};

export { AlertProvider };
export default AlertContext;