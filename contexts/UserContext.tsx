import React, { useEffect, useState } from "react";
import getLocalStorage from "../helpers/getLocalStorage";
import setLocalStorage from "../helpers/setLocalStorage";

const UserStatus = {
  LoggedIn: "LOGGED_IN",
  LoggedOut: "LOGGED_OUT",
};

type Props = {
  children: JSX.Element;
};

const UserContext = React.createContext<any>(null);
UserContext.displayName = "UserContext";

const UserProvider = ({ children }: Props) => {
  const [user, setUser] = useState<any>(() => getLocalStorage("user", null));
  const [userStatus, setUserStatus] = useState(UserStatus.LoggedOut);
  //user ? UserStatus.LoggedIn : UserStatus.LoggedOut
  useEffect(() => {
    setLocalStorage("user", user);
    setUserStatus(user ? UserStatus.LoggedIn : UserStatus.LoggedOut);
  }, [user]);

  return (
    <UserContext.Provider
      value={{
        user: user,
        userStatus: userStatus,
        login: (user: any) => {
          setUser(user);
          setUserStatus(UserStatus.LoggedIn);
        },
        logout: () => {
          setUser(null);
          setUserStatus(UserStatus.LoggedOut);
        },
        clear: () => setUserStatus(UserStatus.LoggedOut),
        setUserStatus,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export { UserProvider };
export default UserContext;
