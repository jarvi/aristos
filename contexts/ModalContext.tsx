import React, { useEffect, useState } from "react";
import Login from "../components/auth/Login";
import ResetPassword from "../components/auth/ResetPassword";
import SignUp from "../components/auth/SignUp";
import InformativeModal from "../components/informativeModal/InformativeModal";


// const ModalStatus = {
//   LoggedIn: "LOGGED_IN",
//   LoggedOut: "LOGGED_OUT",
// };

type Props = {
  children: JSX.Element;
};

const ModalContext = React.createContext<any>(null);
ModalContext.displayName = "ModalContext";

const ModalProvider = ({ children }: Props) => {
  const [showModal, setShowModal] = useState(false);
  const [selectedForm, setSelectedForm] = useState('loginForm')

  return (
    <ModalContext.Provider
      value={{
        selectedForm,
        setSelectedForm,
        showModal,
        setShowModal
      }}
    >
      {children}
    </ModalContext.Provider>
  );
};

export { ModalProvider };
export default ModalContext;
